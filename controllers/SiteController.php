<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Usertype;
use app\models\ContactForm;
use yii\base\Model;
use yii\widgets\ActiveForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'login',
												'logout'
										],
										'allow' => true 
								],
								[ 
										'actions' => [ 
												'logout' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								] 
						],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

   /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest){
       		switch (Yii::$app->user->identity->user_type) {
                  case Usertype::SUPERADMIN:                  
                  return $this->render('superadmin_dashboard.php');
                  break;

                 return $this->redirect(['site/login']);
                 break;
            }
       }else{
         $this->layout = "home_html.php";
    	return $this->render('home');
       }

    }

     /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
     //   $modelContact = new ContactForm();
        $model = new LoginForm();
        // $modelRegister = \Yii::createObject([
        //     'class'    => Registeration::className(),
        // ]); 
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        	switch (Yii::$app->user->identity->user_type) {
            	case Usertype::SUPERADMIN:
                  
                  return $this->render('superadmin_dashboard.php');
                  break;
                  
                  return $this->redirect(['site/login']);
                  break;
        	}
        }

        
        return $this->render('login', [
            'model' => $model,
           
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
      	$this->redirect(['site/login']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
          /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }
}
