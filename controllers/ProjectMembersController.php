<?php

namespace app\controllers;

use Yii;
use app\models\ProjectMembers;
use app\models\Projects;
use app\models\ProjectMembersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
/**
 * ProjectMembersController implements the CRUD actions for ProjectMembers model.
 */
class ProjectMembersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectMembers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectMembersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProjectMembers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjectMembers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=null)
    {
        $model = new ProjectMembers();
        $model->user_id=$this->findMembers($id);
        $findModel=ProjectMembers::find()->where(['project_id'=>Yii::$app->encryptor->decrypt($id)])->asArray()->all();

        if ($model->load(Yii::$app->request->post())) {
            if($id!==null){
                $find=Projects::find()->where(['id'=>Yii::$app->encryptor->decrypt($id)])->one();
                if($find==null){
                      \Yii::$app->session->setFlash('success', \Yii::t('app', 'Project Not found.Try Again'));
                     return $this->redirect(['projects/index']);
                }
            }
            $user_ids=$model->user_id;
             ProjectMembers::deleteAll('project_id = :id', [':id' => Yii::$app->encryptor->decrypt($id)]);
            for ($i=0; $i < count($user_ids); $i++) { 
                 $user_id=$user_ids[$i];;
                 $findUser=User::find()->where(['id'=>$user_id])->one();
                 $members=new ProjectMembers;
                 $model->id=null;
                 $model->user_id=$user_id;
                 $model->project_id=Yii::$app->encryptor->decrypt($id);
                 $model->usertype_id=$findUser->user_type;
                 $model->isNewRecord=true;
                 $model->save(false);
            }
             \Yii::$app->session->setFlash('success', \Yii::t('app', 'Project Members mapped'));
            return $this->redirect(['projects/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
      public function findMembers($id)
    {
         $findModel=ProjectMembers::find()->where(['project_id'=>Yii::$app->encryptor->decrypt($id)])->asArray()->all();
         $retval=[];
         for ($i=0; $i < count($findModel); $i++) { 
            $retval[]=$findModel[$i]['user_id'];
         }
         return $retval;
    }
    /**
     * Updates an existing ProjectMembers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProjectMembers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProjectMembers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectMembers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectMembers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
