<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use app\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
  use yii\helpers\Url;
/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projects();
        $this->performAjaxValidation($model);
        $path=Url::base(true).'/img/project-icon.jpg';
        if ($model->load(Yii::$app->request->post())) {
              $model->logo = UploadedFile::getInstance($model, 'logo');
               if(!empty($model->logo)){

            $model->logo->saveAs('img/'.'P'.$model->name.'.'.$model->logo->extension);
             $model->logo = Url::base(true).'/img/'.'P'.$model->name.'.'.$model->logo->extension;
          }else{
            $model->logo =Url::base(true).'/img/project-icon.jpg';
          }
            //echo "<pre>"; print_r($model->logo); exit;
            $model->start_date=date('Y-m-d',strtotime($model->start_date)); 
            if(!$model->end_date){
                
            $date = new \DateTime($model->end_date); // Y-m-d
            $date->add(new \DateInterval('P30D')); 
            $model->end_date=  $date->format('Y-m-d');
            }else{
                 $model->end_date=date('Y-m-d',strtotime($model->end_date)); 
            }
           
            $model->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'path' => $path,
        ]);
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->start_date=date('m/d/Y',strtotime($model->start_date)); 
        $model->end_date=date('m/d/Y',strtotime($model->end_date)); 
        $path=$model->logo; 
      
        $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post())) {
       
            $model->logo = UploadedFile::getInstance($model, 'logo');
               if(!empty($model->logo)){

            $model->logo->saveAs('img/'.'P'.$model->name.'.'.$model->logo->extension);
             $model->logo = Url::base(true).'/img/'.'P'.$model->name.'.'.$model->logo->extension;
          }else{
            $model->logo =Url::base(true).'/img/project-icon.jpg';
          }
            $model->start_date=date('Y-m-d',strtotime($model->start_date)); 
             if(!$model->end_date){
                
            $date = new \DateTime($model->end_date); // Y-m-d
            $date->add(new \DateInterval('P30D')); 
            $model->end_date=  $date->format('Y-m-d');
            }else{
                 $model->end_date=date('Y-m-d',strtotime($model->end_date)); 
            }
              $model->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
             'path' => $path,
        ]);
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($key)
    {   
        $id=Yii::$app->encryptor->decrypt($key);
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
          /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
           // \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }
}
