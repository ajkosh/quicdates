<?php
namespace app\controllers;

use Yii;
use yii\base\Controller;
use app\models\GFunctions;
use app\models\User;
use app\models\Projects;
use app\models\ProjectSprints;

class ApiController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
				'error' => [
						'class' => 'yii\web\ErrorAction',
				]
		];
	}
	
	public function actionIndex()
	{	
		$decode = Yii::$app->request->bodyParams;

		if($decode == NULL)
			$decode = GFunctions::getJSONRequest();
		
		if(count($decode) > 0){
			if(isset($decode['service']) && $decode['service']!='')
				$service = $decode['service'];
			else
				$service = '';
			
			if(isset($service) && $service!= '')
			{				
				switch($service){
					case "welcome":
						$this->actionWelcome($decode);
						break;
					case "login":
						$this->actionLogin($decode);
						break;
					case "logout":
						$this->actionLogout($decode);
						break;
					case "changePassword":
						$this->actionChangePassword($decode);
						break;
					case "indexProjectsList":
						$this->actionIndexProjectsList($decode);
						break;
					case "projectsList":
						$this->actionProjectsList($decode);
						break;
					case "projectDetails":
						$this->actionProjectDetails($decode);
						break;
					case "editProjectDetails":
						$this->actionEditProjectDetails($decode);
						break;
						
					default:
						GFunctions::getAPIError(404);
				}
			}
			else
			{
				GFunctions::getAPIError(1,array("Service"));
			}
		}
	}
	
	public function actionWelcome($decode)
	{
		echo 'Hi';exit;
	}	
	
	/**
	 * Login to application
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2. Email id/username
	 * 3.Password
	 */
	
	public function actionLogin($decode)
	{
		$decodeParam = array('emailId','password|base64','device_token','device_type');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
		if($filteredRequest['emailId']=="") {
			GFunctions::getAPIError(1,array("Email id"));
		}
		if(!filter_var($filteredRequest['emailId'], FILTER_VALIDATE_EMAIL) === true){ //Email Incorrect
			GFunctions::getAPIError(5,array('Email address'));
		}
		if($filteredRequest['password']=="") {
			GFunctions::getAPIError(1,array("Password"));
		}
		$email_id 			= 	$filteredRequest['emailId'];
		$password 			= 	$filteredRequest['password'];
		$device_token 		= 	$filteredRequest['device_token'];
		$device_type 		= 	$filteredRequest['device_type'];

		$user_model=User::find()->where("email = '$email_id'")->one();
		if($user_model!=NULL)
		{
			if(isset($user_model->status) && $user_model->status!=1)
			{
				GFunctions::getAPIError(14);
			}
				
			if(Yii::$app->security->validatePassword($password,$user_model->password_hash)==false)
			{
				GFunctions::getAPIError(9);
			}
			else
			{
				$access_token				=	Yii::$app->security->generateRandomString();
				$user_model->device_token	=	$device_token;
				$user_model->device_type	=	$device_type;
				$user_model->access_token	=	$access_token;
				if($user_model->save(false))
				{
					$decode['user_id']		=	$user_model->id;
					$decode['access_token']	=	$access_token;
					$response = array("user_id"=>$user_model->id,"access_token"	=>	$access_token);
					GFunctions::getAPISuccess($response);
				}
			}
		}
		else {
			GFunctions::getAPIError(9);
		}
	}
	
	/**
	 * Logout user
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionLogout($decode)
	{
		$decodeParam = array('user_id', 'access_token');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
	
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
	
		$access_token	=	$filteredRequest['access_token'];
		$user_model 	= 	User::find()->where(['id'=>$filteredRequest['user_id'], 'status'=>1])->one();

		if($user_model!=NULL)
		{
			if($access_token==$user_model->access_token)
			{
				$user_model->device_token 	= "";
				$user_model->device_type	= "";
				$user_model->access_token 	= null;
				$user_model->updated_at 	= date('Y-m-d H:i:s');
				if($user_model->save())
				{
					$response=array("message"=>"Logged out successfully");
					GFunctions::getAPISuccess($response);
				}
			}
			else{
				$response=array("message"=>"Logged out successfully.");
				GFunctions::getAPISuccess($response);
			}
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}
	}
	
	/**
	 * Change Password
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionChangePassword($decode)
	{
		$decodeParam 	= 	array('user_id', 'access_token', 'current_password|base64', 'new_password|base64');
		$filteredRequest= 	GFunctions::getAPIDataFilter($decode, $decodeParam);
		if($filteredRequest['current_password']=="") {
			GFunctions::getAPIError(1,array("Current Password"));
		}
		if($filteredRequest['new_password']=="") {
			GFunctions::getAPIError(1,array("New password"));
		}
		if(strlen($filteredRequest['new_password'])<1) { //Password min 8 character
			GFunctions::getAPIError(3,array("New password"));
		}
		if(strlen($filteredRequest['new_password'])>16) { //Password max 16 character
			GFunctions::getAPIError(3,array("New password"));
		}
		/* if (!(preg_match('((?=.*\d)(?=.*[A-Z]))',$filteredRequest['new_password']))) { //Password error
			GFunctions::getAPIError(4,array("New password"));
		} */
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
		if($filteredRequest['access_token']=="") {
			GFunctions::getAPIError(1,array("Access Token"));
		}
		
		$current_password	=	$filteredRequest['current_password'];
		$new_password		=	$filteredRequest['new_password'];
		
		$user_model			=	User::find()
								->select(["id","access_token","password_hash","status"])
								->where(['id'=>$filteredRequest['user_id'], 'access_token'=>$filteredRequest['access_token'], 'status'=>1])
								->one();
		if($user_model != NULL)
		{
            if(Yii::$app->security->validatePassword($current_password,$user_model->password_hash)==false)
            {
            	GFunctions::getAPIError(6,array("Current Password"));
            }
            else
            {
                $user_model->updated_by=	$user_model->id;
                $user_model->updated_at=	date("Y-m-d H:i:s");
                $user_model->password_hash	=	Yii::$app->security->generatePasswordHash($new_password);
                if($user_model->save(false))
                {
                    $response	=	array(	"user_id"		=>	$user_model->id,
                                            "access_token"	=>	$user_model->access_token
                    );
                    GFunctions::getAPISuccess($response);
                }
                else
                {
                     GFunctions::getAPIError(100);
                }
           }
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}
	}
	
	/**
	 * Get Indexed Projects List
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionIndexProjectsList($decode)
	{
		$decodeParam = array('user_id', 'access_token');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
	
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
	
		$access_token	=	$filteredRequest['access_token'];
		$user_model 	= 	User::find()->where(['id'=>$filteredRequest['user_id'], 'status'=>1, 'access_token'=>$access_token])->one();
	
		if($user_model!=NULL)
		{
			$project_details = [];
			$projectDetails = Projects::find()->asArray()->all();
			if(!empty($projectDetails)) {
				foreach($projectDetails as $key=>$details) {
					$project_details[$key]	=	array(	"projecId"=>$details['id'],
														"projectName"=>$details['name'],
														"projectLogo"=>$details['logo']
												);
				}
			}
			GFunctions::getAPISuccess($project_details);
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}
	
	}
	
	/**
	 * Get Projects List
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionProjectsList($decode)
	{
		$decodeParam = array('user_id', 'access_token');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
		
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
		
		$access_token	=	$filteredRequest['access_token'];
		$user_model 	= 	User::find()->where(['id'=>$filteredRequest['user_id'], 'status'=>1, 'access_token'=>$access_token])->one();
		
		if($user_model!=NULL)
		{
			$project_details = [];
			$projectDetails = Projects::find()->joinwith(['projectSprints','projectMembers'=> function ($query){ 
       														$query->joinWith(['projectMembersDetails','projectMembersType']);
     													}])->asArray()->all();

			if(!empty($projectDetails)) {
				foreach($projectDetails as $key=>$details) {
					$project_details[$key]	=	array(	"projectId"			=>	$details['id'],
														"projectName"		=>	$details['name'],
														"projectLogo"		=>	$details['logo'],
														"projectDescription"=>	$details['description'],
														"projectOwner"		=>	$details['owner'],
														"projectStartDate"	=>	$details['start_date'],
														"projectEndDate"	=>	$details['end_date']
													);
					/*
					 * Project Memeber Details
					 */
					$project_members = [];
					if(!empty($details['projectMembers'])) {
						//$i=0;
						foreach($details['projectMembers'] as $memeberKey=>$projectMembers) {
							if($projectMembers['projectMembersType']['role']=="PM") {
								$project_details[$key]['projectManager'] 	= 	$projectMembers['projectMembersDetails']['username'];
								$project_details[$key]['projectManagerId'] 	= 	$projectMembers['projectMembersDetails']['id'];
							}
							//else{
								$project_members[$memeberKey]	=	array(	"userId"		=>	$projectMembers['projectMembersDetails']['id'],
																			"userName"		=>	$projectMembers['projectMembersDetails']['username'],
																			"emailId"		=>	$projectMembers['projectMembersDetails']['email'],
																			"userRoleName"	=>	$projectMembers['projectMembersType']['title'],
																			"userRole"		=>	$projectMembers['projectMembersType']['role'],
																			"otherProjects"	=>	'vocaworks,viva',
																		);
								//$i++;
								//}
						}
					}
					$project_details[$key]['resorceNames'] = $project_members;
					
					/*
					 * Spint Details
					 */
					$project_sprints = [];
					if(!empty($details['projectSprints'])) {
						foreach($details['projectSprints'] as $sprintKey=>$projectSprints) {
							$project_sprints[$sprintKey]	=	array(	"sprintName"	=>	$projectSprints['sprint_name'],
																		"startDate"		=>	$projectSprints['start_date'],
																		"endDate"		=>	$projectSprints['end_date'],
																		"currentSprint"	=>	$projectSprints['status']==1 ? 'yes' : 'no',
																);
						}
					}
					$project_details[$key]['projectSprints'] = $project_sprints;
					/*
					 * Other Details
					 */
					$project_details[$key]['otherDetails']	=	array(	"weeklyCallTime" 	=> 	$details['weekly_call_time'],
																		"platform" 			=> 	$details['platform'],
																		"appFSLink" 		=> 	$details['app_fs_link'],
																		"appTestCaseLink" 	=> 	$details['app_test_case_link'],
																		"appWireframeLink" 	=> 	$details['app_wireframe_link'],
																		"appInvisionLink" 	=> 	$details['app_invision_link'],
																		"webFSLink" 		=> 	$details['web_fs_link'],
																		"webTestCaseLink" 	=> 	$details['web_test_case_link'],
																		"webWireframeLink" 	=> 	$details['web_wireframe_link'],
																		"webInvisionLink" 	=> 	$details['web_invision_link'],
																);
					
				}
			}
			GFunctions::getAPISuccess($project_details);
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}		
	}
	
	/**
	 * Get Projects Details
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionProjectDetails($decode)
	{
		$decodeParam = array('user_id', 'access_token','projectId');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
	
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
	
		if($filteredRequest['projectId']=="") {
			GFunctions::getAPIError(1,array("Project id"));
		}
		
		$access_token	=	$filteredRequest['access_token'];
		$user_model 	= 	User::find()->where(['id'=>$filteredRequest['user_id'], 'status'=>1, 'access_token'=>$access_token])->one();
	
		if($user_model!=NULL)
		{
			$project_details = [];
			$projectDetails = Projects::find()->where(['projects.id'=>$filteredRequest['projectId']])->joinwith(['projectSprints','projectMembers'=> function ($query){
				$query->joinWith(['projectMembersDetails','projectMembersType']);
			}])->asArray()->all();
	
			if(!empty($projectDetails)) {
				foreach($projectDetails as $key=>$details) {
					$project_details[$key]	=	array(	"projectId"			=>	$details['id'],
														"projectName"		=>	$details['name'],
														"projectLogo"		=>	$details['logo'],
														"projectDescription"=>	$details['description'],
														"projectOwner"		=>	$details['owner'],
														"projectStartDate"	=>	$details['start_date'],
														"projectEndDate"	=>	$details['end_date']
												);
					/*
					 * Project Memeber Details
					 */
					$project_members = [];
					if(!empty($details['projectMembers'])) {
						//$i=0;
						foreach($details['projectMembers'] as $memeberKey=>$projectMembers) {
							if($projectMembers['projectMembersType']['role']=="PM") {
								$project_details[$key]['projectManager'] 	= 	$projectMembers['projectMembersDetails']['username'];
								$project_details[$key]['projectManagerId'] 	= 	$projectMembers['projectMembersDetails']['id'];
							}
							//else{
							$project_members[$memeberKey]	=	array(	"userId"		=>	$projectMembers['projectMembersDetails']['id'],
																		"userName"		=>	$projectMembers['projectMembersDetails']['username'],
																		"emailId"		=>	$projectMembers['projectMembersDetails']['email'],
																		"userRoleName"	=>	$projectMembers['projectMembersType']['title'],
																		"userRole"		=>	$projectMembers['projectMembersType']['role'],
																		"otherProjects"	=>	'vocaworks,viva',
																);
							//$i++;
							//}
						}
					}
					$project_details[$key]['resorceNames'] = $project_members;
						
					/*
					 * Spint Details
					 */
					$project_sprints = [];
					if(!empty($details['projectSprints'])) {
						foreach($details['projectSprints'] as $sprintKey=>$projectSprints) {
							$project_sprints[$sprintKey]	=	array(	"sprintName"	=>	$projectSprints['sprint_name'],
																		"startDate"		=>	$projectSprints['start_date'],
																		"endDate"		=>	$projectSprints['end_date'],
																		"currentSprint"	=>	$projectSprints['status']==1 ? 'yes' : 'no',
																);
						}
					}
					$project_details[$key]['projectSprints'] = $project_sprints;
					/*
					 * Other Details
					 */
					$project_details[$key]['otherDetails']	=	array(	"weeklyCallTime" 	=> 	$details['weekly_call_time'],
																		"platform" 			=> 	$details['platform'],
																		"appFSLink" 		=> 	$details['app_fs_link'],
																		"appTestCaseLink" 	=> 	$details['app_test_case_link'],
																		"appWireframeLink" 	=> 	$details['app_wireframe_link'],
																		"appInvisionLink" 	=> 	$details['app_invision_link'],
																		"webFSLink" 		=> 	$details['web_fs_link'],
																		"webTestCaseLink" 	=> 	$details['web_test_case_link'],
																		"webWireframeLink" 	=>	$details['web_wireframe_link'],
																		"webInvisionLink" 	=> 	$details['web_invision_link'],
																);
						
				}
			}
			GFunctions::getAPISuccess($project_details);
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}
	}
	
	/**
	 * Edit Projects Details
	 * @param
	 * 1.service -
	 * This variable is used for security purpose.
	 * If value of this varible is blank then display 404-Page not found in response.	 *
	 * 2.user-id- logged in user id
	 * 3.access token - logged in user token
	 */
	public function actionEditProjectDetails($decode)
	{
		$decodeParam = array('user_id', 'access_token', 'projectId','weeklyCallTime','projectStartDate','projectEndDate','platform','appFSLink','appTestCaseLink','appTestCaseLink','appWireframeLink',
				'appInvisionLink','webFSLink','webTestCaseLink','webWireframeLink','webInvisionLink','sprintName','startDate','endDate');
		$filteredRequest = GFunctions::getAPIDataFilter($decode, $decodeParam);
	
		if($filteredRequest['user_id']=="") {
			GFunctions::getAPIError(1,array("User id"));
		}
		
		if($filteredRequest['projectId']=="") {
			GFunctions::getAPIError(1,array("Project Id"));
		}
	
		$access_token	=	$filteredRequest['access_token'];
		$user_model 	= 	User::find()->where(['id'=>$filteredRequest['user_id'], 'status'=>1, 'access_token'=>$access_token])->one();
	
		if($user_model!=NULL)
		{			
			$projectId 			= 	$filteredRequest['projectId'];
			$projectStartDate	= 	$filteredRequest['projectStartDate'];
			$projectEndDate		= 	$filteredRequest['projectEndDate'];
			$weekly_call_time	= 	$filteredRequest['weeklyCallTime'];
			$platform 			= 	$filteredRequest['platform'];
			$app_fs_link 		= 	$filteredRequest['appFSLink'];
			$app_test_case_link = 	$filteredRequest['appTestCaseLink'];
			$app_wireframe_link	= 	$filteredRequest['appWireframeLink'];
			$app_invision_link 	= 	$filteredRequest['appInvisionLink'];
			$web_fs_link 		= 	$filteredRequest['webFSLink'];
			$web_test_case_link = 	$filteredRequest['webTestCaseLink'];
			$web_wireframe_link = 	$filteredRequest['webWireframeLink'];
			$web_invision_link 	= 	$filteredRequest['webInvisionLink'];
			
			$sprintName 	= 	$filteredRequest['sprintName'];
			$start_date 	= 	$filteredRequest['startDate'];
			$end_date 		= 	$filteredRequest['endDate'];
			
			$projectDetails = 	Projects::find()->where(['id'=>$projectId])->one();
			
			if(!empty($projectDetails)) {
				$projectDetails['start_date'] 			= $projectStartDate==null || $projectStartDate=="" ? $projectDetails['start_date'] : date("Y-m-d",strtotime($projectStartDate));
				$projectDetails['end_date'] 			= $projectEndDate==null || $projectEndDate=="" ? $projectDetails['end_date'] : date("Y-m-d",strtotime($projectEndDate));
				$projectDetails['weekly_call_time'] 	= $weekly_call_time==null || $weekly_call_time=="" ? $projectDetails['weekly_call_time'] : $weekly_call_time;
				$projectDetails['platform'] 			= $platform==null || $platform=="" ? $projectDetails['platform'] : $platform;
				$projectDetails['app_fs_link'] 			= $app_fs_link==null || $app_fs_link=="" ? $projectDetails['app_fs_link'] : $app_fs_link;
				$projectDetails['app_test_case_link'] 	= $app_test_case_link==null || $app_test_case_link=="" ? $projectDetails['app_test_case_link'] : $app_test_case_link;
				$projectDetails['app_wireframe_link'] 	= $app_wireframe_link==null || $app_wireframe_link=="" ? $projectDetails['app_wireframe_link'] : $app_wireframe_link;
				$projectDetails['app_invision_link'] 	= $app_invision_link==null || $app_invision_link=="" ? $projectDetails['app_invision_link'] : $app_invision_link;
				$projectDetails['web_fs_link'] 			= $web_fs_link==null || $web_fs_link=="" ? $projectDetails['web_fs_link'] : $web_fs_link;
				$projectDetails['web_test_case_link'] 	= $web_test_case_link==null || $web_test_case_link=="" ? $projectDetails['web_test_case_link'] : $web_test_case_link;
				$projectDetails['web_wireframe_link'] 	= $web_wireframe_link==null || $web_wireframe_link=="" ? $projectDetails['web_wireframe_link'] : $web_wireframe_link;
				$projectDetails['web_invision_link'] 	= $web_invision_link==null || $web_invision_link=="" ? $projectDetails['web_invision_link'] : $web_invision_link;
				
				if($sprintName!=null || $sprintName!="") {
					if($start_date==null || $start_date==''){
						GFunctions::getAPIError(1,array("current sprint start date"));
					}
					if($end_date==null || $end_date==''){
						GFunctions::getAPIError(1,array("current sprint end date"));
					}
					
					$projectSprint = ProjectSprints::find()->where(['project_id'=>$projectId,'status'=>1])->orderBy(['id'=>SORT_DESC])->one();
					
					if(!empty($projectSprint)) {
						$projectSprint->status = 0;
						if($projectSprint->save(false)) {
							$project_sprint 			= 	new ProjectSprints();
							$project_sprint->project_id = 	$projectId;
							$project_sprint->sprint_name= 	$sprintName;
							$project_sprint->start_date = 	date('Y-m-d',strtotime($start_date));
							$project_sprint->end_date 	= 	date('Y-m-d',strtotime($end_date));
							$project_sprint->status 	= 	1;
							$project_sprint->created_by = 	$filteredRequest['user_id'];
							$project_sprint->save(false);
						}
					}
					else{
						$project_sprint 			= 	new ProjectSprints();
						$project_sprint->project_id = 	$projectId;
						$project_sprint->sprint_name= 	$sprintName;
						$project_sprint->start_date = 	date('Y-m-d',strtotime($start_date));
						$project_sprint->end_date 	= 	date('Y-m-d',strtotime($end_date));
						$project_sprint->status 	= 	1;
						$project_sprint->created_by = 	$filteredRequest['user_id'];
						$project_sprint->save(false);
					}
				}
				else{
					$projectDetails->save();
				}
			}
			$viewDecode['user_id'] 		= 	$filteredRequest['user_id'];
			$viewDecode['access_token'] = 	$filteredRequest['access_token'];
			$viewDecode['projectId'] 	= 	$filteredRequest['projectId'];
			$this->actionProjectDetails($viewDecode);
			//GFunctions::getAPISuccess(array("Details updated."));
		}
		else
		{
			$response=array("User not found.");
			GFunctions::getAPIError(402,array("User"));
		}
	}
}
