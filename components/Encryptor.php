<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Mailer.
 *
 * @author AJ
 */
class Encryptor extends Component
{
    public $output = false;
    public $encrypt_method = "AES-256-CBC";
    //pls set your unique hashing key
    public $secret_key = 'qerty1209';
    public  $secret_iv = '1209qwerty';
   

   public function encrypt( $string) {
              // hash
         $key = hash('sha256', $this->secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $this->secret_iv), 0, 16);
        $output = openssl_encrypt($string, $this->encrypt_method, $key, 0, $iv);
         return $output = base64_encode($output);
    }

     public function decrypt( $string) {
         // hash
        $key = hash('sha256', $this->secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
         $iv = substr(hash('sha256', $this->secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $this->encrypt_method, $key, 0, $iv);
        return $output;
    }

      
}