<?php
$s3_set	=	'N';
if($s3_set=='Y')
{
	/* Image path used in system for s3 images */
	$s3_path			=	'https://s3-us-west-2.amazonaws.com/quipdates-dev/web/img';
	$image_save_path	=	'web/img';
	$dir_path			=	'var/www/html';
}
else{
	/* Image path used in system for non s3 images */
	$s3_path			=	__DIR__ . '/../web/img';
	$image_save_path	=	__DIR__ . '/../web/img';
	$dir_path			=	__DIR__ . '/../web/img';
}

$live_path = 'http://localhost/quipdates';
$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "";

if(stristr($host, "localhost") == FALSE)
{
	$live_path = 'http://quicpdates.us-west-2.elasticbeanstalk.com/quicdates';
}

return [ 
		'adminEmail'					=> 'nileshy@arkenea.com',
		'ccAdminEmail' 					=> 'nileshy@arkenea.com',
		'contact_support_email' 		=> 'nileshy@arkenea.com',
		'support_email' 				=> 'nileshy@arkenea.com',
		'super_admin_domain' 			=> 'myosapp.com',
		'emailFrom' 					=> 'Team Quipdates',
		'site_title' 					=> 'Quipdates',
		'live_path' 					=> $live_path,
		'directory_path' 				=> __DIR__ . "/..",
		'dir_path'						=> $dir_path,
		'smtp_mail' 					=> 'N',
		's3_set' 						=> $s3_set,
		's3_path'						=> $s3_path,
		'image_save_path'				=> $image_save_path
];