<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Employee;
use app\models\Guardians;


/**
 * Base Model class for all generic function for model
 * and also model constant in all models
 */
class BaseModel extends \yii\db\ActiveRecord
{
  const ACTIVE = 1;
  const INACTIVE = 0;
  const DISABLE = 0;
  const ENABLE = 1;
  const ISNOTDELETE = 1;
  const ISDELETE = 0;
  const EMAIL = 1;
  const PHONE = 2;
  const YES=0;
  const NO=1;
  //user realted constant
  const SUPERADMIN = 1;
  const TRAINER=2;
  const EMPLOYER=3;
  
  
  // For Checkbox 
  const CHECKED=1;
  const UNCHECKED=0;
 //For work days

  const DEFAULT_ASSET_ID=1;

 
  /**
   * [getDropDowData generic function to get drop down data]
   *  in the respective model there should be a function called
   *  getFullName which should return the value for drop down
   * @return [array]
   */
  public static function getDropDownData($params=null)
  {
    $res = ArrayHelper::map(self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->all(), 'id', "fullName");

    //Sorting based on value
    asort($res);

    return $res;

  }
    /**
     * [getFullName get fullname for drop down ]
     * @return [string]
     */
    public function getFullName()
    {
        return $this->title;
    } 


 /**
     * htmlstatus is generic function to get html view for status
     * @return [string]
     *@author name AJ
     *@param $id integer
     *       $size [sm,lg,xs,xlg]
     *       $color[success,danger,primary,warning,grey.light,pink,yellow,purple]
     *       $icon string [font-awesome]
     *       $type string [title]
     */
     public static function gethtmlstatus($size=null,$color=null,$icon=null,$type=null)
    {    
          
       return '<span class="label label-'.$size.' label-'.$color.' arrowed-in"><i class="ace-icon fa fa-'.$icon.' bigger-120"></i>'.Yii::t('app',$type).'</span>';
         
    }
    

     /**
     * gettitle use for get value without  activedataprovider 
     * Use directly with pass id            
     * @return [string]
     */
     public static function fetchTitle($id)
    {
        $value=Self::findOne($id);
        return $value['title'];
    }

    /**
     * @return string
     */
    public static function getFlashMessage($param)
    {
        if ($param==Self::TYPE_CONFIRMATION) {
            return \Yii::t('app', 'A message has been sent to your email address. It contains your password and a confirmation link that you must click to complete registration.');
        } else if ($param==Token::TYPE_INVITATION) {
            return \Yii::t('app', 'Invitation link is invalid or expired.Pleas request to your sender again');
        } else if ($param==Self::REG) {
            return \Yii::t('app', 'A message has been sent to email address. It contains a confirmation link that user must click to complete registration.');
        } else {
            return \Yii::t('app', 'Welcome! Registration is complete.');
        }
    }

    /**
     * @param array strict 
     * @author AJ
     * @return bool Whether the user is blocked to any click,event,button or field based on user_type.
     */
    public static function isBlocked(array $ut)
    {   
      $user_type=Yii::$app->user->identity->user_type;
      if (in_array($user_type, $ut)) {
        return true;
      }
      
    }

 
      
        

}