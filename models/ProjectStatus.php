<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_status".
 *
 * @property int $id
 * @property int $status_id
 * @property int $project_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $is_deleted 1=not_delete, 0=deleted
 * @property int $status 1=active, 0=inactive
 */
class ProjectStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_id', 'project_id', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', '1=not_delete, 0=deleted'),
            'status' => Yii::t('app', '1=active, 0=inactive'),
        ];
    }
}
