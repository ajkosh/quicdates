<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $status
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name','start_date','description'], 'required'],
            [['start_date', 'end_date', 'created_at', 'updated_at','weekly_call_time','weekly_call_time','web_wireframe_link','web_invision_link','web_test_case_link','app_test_case_link','app_wireframe_link','app_invision_link','web_fs_link','work_day_id','logo'], 'safe'],
            [['created_by', 'updated_by', 'status'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 100],
             ['end_date', 'compareDates'],
        ];
    }
    
      public function compareDates($attribute, $params) 

    {

     $end_date = strtotime($this->end_date);

     $start_date = strtotime($this->start_date);

        if ($end_date < $start_date) {

                $this->addError($attribute, Yii::t('app', ''.$this->end_date.' is not valid '));

        }

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'logo' => Yii::t('app', 'Logo'),
            'description' => Yii::t('app', 'Description'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
	public function getProjectMembers()
    {
      return $this->hasMany(ProjectMembers::className(), ['project_id' => 'id']);
   	}
   	
   	public function getProjectSprints()
   	{
   		return $this->hasMany(ProjectSprints::className(), ['project_id' => 'id']);
   	}
}
