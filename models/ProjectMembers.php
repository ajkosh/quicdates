<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_members".
 *
 * @property int $id
 * @property int $user_id
 * @property int $project_id
 * @property int $project_role_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $is_deleted 1=not_delete, 0=deleted
 * @property int $status 1=active, 0=inactive
 */
class ProjectMembers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_members';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'],'required'],
            [['user_id', 'project_id', 'usertype_id', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Members'),
            'project_id' => Yii::t('app', 'Project ID'),
            'usertype_id' => Yii::t('app', 'Project Role ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', '1=not_delete, 0=deleted'),
            'status' => Yii::t('app', '1=active, 0=inactive'),
        ];
    }
    
    public function getProjectMembersDetails()
    {
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getProjectMembersType()
    {
    	return $this->hasOne(Usertype::className(), ['id' => 'usertype_id']);
    }
}
