<?php
$error[1] 	= "Please enter data for {1}."; 
$error[2] 	= "You are already registered. Please log in to access your account.";
$error[3] 	= "{1} should be mimimum 1 and maximum 16 characters long.";
$error[4] 	= "{1} should contain at least one number and Capital letter.";
$error[5] 	= "Incorrect {1} format.";
$error[6] 	= "{1} does not match.";
$error[7] 	= "{1} is not available.";
$error[8] 	= "{1} is not allowed.";
$error[9] 	= "Incorrect email or password. Please try again.";
$error[10] 	= "Email already registered from {1}. Login from {1} to access your account.";
$error[11] 	= "You have tried 5 times wrong email or password. Please use forgot password service.";
$error[12] 	= "Social media id should not be empty.";
$error[13] 	= "Social media type should not be empty.";
$error[14] 	= "Your account is inactive.Please contact to Admin.";
$error[15] 	= "You are not registered. Please sign up to create a new account.";
$error[17] 	= "{1}";
$error[18] 	= "{1}";
$error[19]	=	"{1} already exists.";
$error[20]	= "{1} is empty. Please set your {1}";
/* Global errors */
$error[100] = "Oops, something is not right. Please try again.";
$error[101] = "Notification not sent.";
$error[401] = "{1} not found.";
$error[402] = "{1} not found.";
$error[403] = "{1} not found.";
$error[404] = "Service not found.";

