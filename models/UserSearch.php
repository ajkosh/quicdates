<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Usertype;



/**
 * SocialPluginsSearch represents the model behind the search form about `app\models\SocialPlugins`.
 */
class UserSearch extends User
{
    public $fullName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['username', 'password_hash', 'auth_key', 'created_at', 'updated_at','user_type','email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = User::find()
         ->joinwith(['userType'])->where(['not in','user_type',[User::SUPERADMIN]]);
         
      
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
         
          //  'email' => $this->email,
            // 'last_name' => $this->last_name,
           // 'fullName'=>$this->first_name.$this->last_name,
           
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', ''.Usertype::tableName().'.id', $this->user_type])
             ->andFilterWhere(['like', 'email', $this->email]);
          

        return $dataProvider;
    }
}
