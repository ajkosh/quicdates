<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\GFunctions;
use app\models\Users;
use app\models\PromoType;
use app\models\PromoCodes;
use app\models\UserRoles;
use app\models\GlobalSettings;
use Twilio\Rest\Client;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Account;

error_reporting(E_ALL);
ini_set('display_errors', 1);

class GFunctions extends ActiveRecord
{
	/**
	 * Send IOS push notification
	 * @param array $req_arr['device_token',user_id,flag,msg]
	 */
	public static function sendApn($req_arr)
	{	
		GFunctions::writeResponceAction($req_arr);
		$token = $req_arr['device_token']; //user device token	
		
		if($token!="" && $token!="0")
		{
			$apnsHost 	=	'gateway.push.apple.com'; // live environment url
			$apnsCert 	=	'myos_apns_dist.pem'; //certificate  apns-dev.pem
			//$apnsHost 	=	'gateway.sandbox.push.apple.com'; // dev environment url
			//$apnsCert 	=	'myos_apns_dev.pem'; //certificate  apns-dev.pem
			$apnsPort 	=	2195; //apple server port
			$token		=	$req_arr['device_token']; //user device token			
	
			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		}
	
		try
		{
			$notification_count=1;
			if(isset($req_arr['user_id']) && $req_arr["user_id"]!="")
			{
				$profile_model		=	Users::find()
										->select(["user_id","badge_count","session_request_accept","session_start_complete","session_time_slot","session_cancel_trainer","user_no_show"])
										->where(["user_id"=>$req_arr['user_id']])->one();				
				//$notification_count	=	(int)$profile_model->badge_count+1;
				//$profile_model->badge_count=$notification_count;
				//$profile_model->update(false);
			}
			elseif(isset($req_arr['trainer_id']) && $req_arr["trainer_id"]!="")
			{
				$profile_model		=	Trainers::find()->select(["trainer_id","badge_count"])->where(["trainer_id"=>$req_arr['trainer_id']])->one();				
				//$notification_count	=	(int)$profile_model->badge_count+1;
				//$profile_model->badge_count=$notification_count;
				//$profile_model->update(false);
			}
			else{
				$notification_count=1;
			}
			$notification_count=1;
			if($req_arr["flag"]== "CreateSession")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "NS","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "SessionAccept")
			{
				if(isset($profile_model->session_request_accept) && $profile_model->session_request_accept=="Y")
				{
					$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SA","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
				}
				else{					
					return true;
					exit;
				}
			}
			elseif($req_arr["flag"]== "SessionStart")
			{
				if(isset($profile_model->session_start_complete) && $profile_model->session_start_complete=="Y")
				{
					$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SS","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
				}
				else{
					return true;
					exit;
				}
			}
			elseif($req_arr["flag"]== "SessionStop")
			{
				if(isset($profile_model->session_start_complete) && $profile_model->session_start_complete=="Y")
				{
					$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SC","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
				}
				else{
					return true;
					exit;
				}
			}
			elseif($req_arr["flag"]== "SessionReject")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SR","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "UserCancel")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "CBU","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "TrainerCancel")
			{
				if(isset($profile_model->session_cancel_trainer) && $profile_model->session_cancel_trainer=="Y")
				{
					$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "CBT","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
				}
				else{
					return true;
					exit;
				}
			}
			elseif($req_arr["flag"]== "RescheduleSession")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "URS","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "RescheduleAccept")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SRA","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "RescheduleReject")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SRR","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "ExtendSession")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "UES","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "ExtendAccept")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SEA","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "ExtendReject")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SER","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "UserRateTrainer")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "URT","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "SessionNotAccept")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "SNA","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "UserNoshow")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "NSC","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "TrainerNoshow")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "NST","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			elseif($req_arr["flag"]== "UpcomingSession")
			{
				$payload["aps"] 	= 	array("alert" => $req_arr["msg"],"notification_type" => "UCS","flag" => "1", "badge" => $notification_count, "sound" => "default","sweat_session_id"=>$req_arr["id"]);
			}
			
			if($token!="" && $token!="0" && (strlen($token)>20) && !empty($payload["aps"]))
			{
				$apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 20, STREAM_CLIENT_CONNECT, $streamContext);
	
				$output = json_encode($payload);
				$token = pack('H*', str_replace(' ', '', $token));
				$apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;
	
				fwrite($apns, $apnsMessage);
				//@socket_close($apns);
				@fclose($apns);
				return true;	
			}
		}
		catch (Exception $e) {
			echo "other error";
			return false;
			// Something else happened, completely unrelated to Stripe
		}
	
	}	
	
	
	/**
	 * Send push notification to android device
	 * @param array $req_arr['device_token',user_id, flag, msg]
	 */
	public static function sendGCM($req_arr)
	{
		$reg = $req_arr['device_token'];
	
		if($reg!="" && $reg!="0")
		{
			$url = 'https://android.googleapis.com/gcm/send';
			//$serverApiKey = "AIzaSyD8um-8pr7G1DNUzYMsl8mqdpOab5YSBHY";
			$serverApiKey = "AIzaSyDaF1z9vtDFsgg5KY-iMqNWfD9gsGHcBnY";
			$headers = array('Content-Type:application/json','Authorization:key=' . $serverApiKey);
		}
		$profile_model=Users::findOne($req_arr['user_id']);
		$notification_count=(int)$profile_model->notification_count+1;
		// Signup notification
		if($req_arr['flag']=="SignupNotification" )
		{
			$data = array('registration_ids' => array($reg),'data' => array('alert' =>$req_arr['msg'],"flag"=>"1", 'badge' => $notification_count, 'sound' => 'default'));
		}
		
		$profile_model->notification_count=$notification_count;
		$profile_model->update();

			
		if($reg!="" && $reg!="0")
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			if ($headers)
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	
				$response = curl_exec($ch);
				curl_close($ch);
		}
			
		
	}
	
	/**
	 * Write request action logs 
	 * @param string $service 
	 */
	public static function writeRequestAction($service,$api)
	{
		//echo $service;exit;
		$myfile = fopen("logs/logs.txt", "a") or die("Unable to open file!");
		$txt = "Api call :-".$api."\r\n";
		$txt .= "Request Action :- ".$service.", Datetime :-".date("Y-m-d H:i:s")."\r\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}

	/**
	 * Write request action logs
	 * @param string $service
	 */
	public static function writeRequestParameter($parameter,$api)
	{
		//echo $service;exit;
		$myfile = fopen("logs/logs.txt", "a") or die("Unable to open file!");
		$txt = "Api call :-".$api."\r\n";
		$txt .= "Request Action :- ".json_encode($parameter).", Datetime :-".date("Y-m-d H:i:s")."\r\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}
	
	/**
	 * Write responce action logs
	 * @param string $responce
	 */
	public static function writeResponceAction($responce)
	{
		//print_r($responce);exit;
		$myfile = fopen("logs/logs.txt", "a") or die("Unable to open file!");
		$txt = "Responce :- ".json_encode($responce).", Datetime :-".date("Y-m-d H:i:s")."\r\n";
		$txt	.=	"------\r\n";
		$txt	.=	"------\r\n";
		$txt	.=	"------\r\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}
	
	/**
	 * send email
	 * @param string $from
	 * @param string $to
	 * @param string $subject
	 * @param string $content
	 * @param string $cc
	 * @param string $FromName
	 * @param string $ReplyTo
	 * @return number
	 */
	public static function sendEmail($from,$to,$subject,$content,$cc = NULL,$FromName="MYOS",$ReplyTo=NULL){
		
		if(Yii::$app->params['smtp_mail']=='Y'){
			$result = Yii::$app->mail->compose(['htmlLayout' => 'layouts/html'])
								//->setFrom($from)
                                ->setFrom([$from => $FromName])
								->setTo($to)//->setTextBody($content)
								//->setCc($cc)
								->setHtmlBody($content)
								->setSubject($subject)
								->send();
			if($result){
				return 1;
			}
		}
		else {
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= "From: ".$from . "\r\n";
			if(mail($to,$subject,$content,$headers))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	/**
	 * get setting values
	 * @param string $code
	 * @return string $setting value
	 */
	public static function getSettingDetails($code){
		
		$settings=GlobalSettings::findOne(['global_key'=>$code]);
		return $settings!=NULL? $settings->value:"";
	}
	
	
	public static function getSupportDetsils()
	{
		$support_email_id 	= 	"SUPPORT_EMAIL_ID";
		$support_contact_no	=	"SUPPORT_CONTACT_NO";
		$values 			= 	[$support_contact_no,$support_email_id];
		$global_model		=	GlobalSettings::find()->select(["global_key","value"])->where(["IN", "global_key",$values])->all();
	
		$responce['support_contact_no']	=	'';
		$responce['support_email_id']	=	'';
		if(!empty($global_model))
		{
			foreach ($global_model as $details)
			{
				if($details->global_key==$support_contact_no)
					$responce['support_contact_no']	=	$details->value;
					else
						$responce['support_email_id']	=	$details->value;
			}
		}
	
		return $responce;
	}
	
	
	/**
	 *
	 * get unique file name extracting special characters and spaces
	 * @param string $filename
	 * @return $file_name
	 */
	public static function getUniqueFileName($filename){
	
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		$name=	pathinfo($filename, PATHINFO_FILENAME);
		$name= preg_replace("/[^ \w]+/", "", trim(strtolower($name)));
		$name= preg_replace("/\s\s+/", "_", $name);
		$name=str_replace(" ", "_", $name);
		$name= preg_replace("/_+/", "_", $name);
		$logo_file=$name.'_'.uniqid().".".$ext;
	
		return $logo_file;
	}

	/**
	 * convert array to object
	 * @param array $array
	 * @return object
	 */
	public static function arrayToObject($array)
	{
	  foreach($array as $key => $value)
    {
        if(is_array($value))
        {
            $array[$key] = self::array_to_object($value);
        }
    }
    return (object)$array;
	}
	
	

	
	//function for genarating number
	public static function getRandomNumber($length = 10)
	{
		//$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		$chars = array_merge(range(0,9));
		shuffle($chars);
		$password = implode(array_slice($chars, 0, $length));
		return $password;
	}//end of function
	
	//function for genarating alpha number
	public static function getRandomAlphaNumber($length = 10)
	{
		$chars = array_merge(range(0,9), range('A','Z'));
		shuffle($chars);
		$password = implode(array_slice($chars, 0, $length));
		return $password;
	}//end of function
	
	public static function getAdminName($admin_id)
	{
		$admin_details = Admin::find()->select('first_name,last_name')->where(" admin_id = $admin_id")->one();
		if(count($admin_details) > 0)
			return $admin_details->first_name." ".$admin_details->last_name ;
		 else 
		 	return "-";
	}
	
	public static function getTrainerName($trainer_id)
	{
		$trainer_details = Trainers::find()->select('first_name,last_name')->where(" trainer_id = $trainer_id")->one();
		if(count($trainer_details) > 0)
			return $trainer_details->first_name." ".$trainer_details->last_name ;
			else
				return "-";
	}
	
	public static function getUserName($user_id)
	{
		$user_details = Users::find()->select('first_name,last_name')->where(" user_id = $user_id")->one();
		if(count($user_details) > 0)
			return $user_details->first_name." ".$user_details->last_name ;
			else
				return "-";
	}
	
	public static function getAdminList()
	{
		$admin_details = Admin::find()->select('email_id')->where(" user_role_id = 0")->all();
		$admin_emails = [];
		foreach($admin_details as $admin)
		{
			array_push($admin_emails,$admin->email_id);
		}
		return $admin_emails;
	}
	/**
     * 
     * Check if sitemap generate & metavalues management flags are on
     * We will pass session email to checkForArkeneaUser() function.
     * This function will check for arkenea domain if yes it will return true else return false.
     * If we get result true from this function we will display menu.  
     */
	public static function checkForArkeneaUser($module_name)
	{
		
		//$module_flag = Yii::$app->params[$module_name];
		if(isset(Yii::$app->user->identity->email_id) && Yii::$app->user->identity->email_id!=''){
			$domain = GFunctions::getDomainFromEmail(Yii::$app->user->identity->email_id);
		}
		else
			$domain = '';
		
		if($domain == Yii::$app->params['super_admin_domain'] )
			return true;
		else 
			return false;
		//return true;
	}
	
	public static function getDomainFromEmail($email)
	{
		if($email!="")
		{
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)=== false) {
				list($user,$domain) = explode('@', $email);//substr($email, 0, strrpos($email, '@'));
				return $domain;
				//print_r($domain);exit;
			}
		}			
	}
	
	
	// get table record dynamic
	public static function getListData($model_name,$table_name,$list_ids,$where_field,$retrieve_field)
	{
		$list = '';
		$data = array();
		
		if($list_ids != '')
		{
			$field_array = explode(",", $retrieve_field);
			foreach($field_array as $key=>$val){
				$field_arraylist[] = "$val";
			}
			$retrieve_field = implode(",",$field_arraylist);
			
			$result=Users::find()->select([$retrieve_field])
			->where("$where_field in(".$list_ids.")")
			->all();
			
			if($result != NULL)
			{
				foreach($result as $key)
				{
					$data[] = $key->$retrieve_field;
				}
				$list = implode(',',$data);
			}
		}
		return $list;
	}
	
	// get title from promo_type table
	public static function getPromoTypeName($promo_type_id,$field=NULL)
	{
		$PromoType_details=PromoType::find()->where(['promo_type_id'=>$promo_type_id])->one();
		$value=NULL;
		if($PromoType_details!=NULL)
		{
			if($field!="")
				$value=$PromoType_details->$field;
				else
					$value=$PromoType_details;
		}
		return $value;
	}
	// get promo_types
	public static function getPromoTypes()
	{
		$PromoTypes=PromoType::find()->all();
		$types = array(""=>"All");
		if($PromoTypes!=NULL)
		{
			foreach($PromoTypes as $key)
			{
				$types[$key->promo_type_id] = $key->promo_type_name;
			}
		}
		return $types;
	}

	// generate random code 
	public static function generateRandomCode($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	//to verify Promo Code
	public static function verifyPromoCode($filterRequest)
	{
		$promo_code 	= $filterRequest['promo_code'];
		$user_id 		= $filterRequest['user_id'];
		$total_amount 	= $filterRequest['total_amount'];
		
		$promo_details=PromoCodes::find()->select(["promo_code","valid_from","valid_till","discount_type","discount_percentage","discount_amount","min_order_amt","usage_count","send_to_users","promo_codes.status as status","users.email_id as email_id"])
									 ->where("BINARY promo_code = '$promo_code'")
									 ->leftJoin("users","users.user_id = $user_id")									 
									 ->one();
		if(count($promo_details) == 0)
		{
			GFunctions::getAPIError(17,array('This Promo Code is not Valid.'));
		}
		if(strtolower($promo_details->status) == 'draft')
		{
			GFunctions::getAPIError(17,array('This Promo Code is not yet available.'));
		}
		if($promo_details->valid_from > date("Y-m-d H:i:s"))
		{
			GFunctions::getAPIError(17,array('This Promo Code is not activated yet.'));
		}
		if($promo_details->valid_till < date("Y-m-d H:i:s") && $promo_details->valid_till != ""  && $promo_details->valid_till != "0000-00-00 00:00:00" )
		{
			GFunctions::getAPIError(17,array('This Promo Code is expired.'));
		}
		if( $promo_details->min_order_amt > $filterRequest['total_amount'] && $filterRequest['total_amount'] != 0  && $filterRequest['total_amount'] != null)
		{
			GFunctions::getAPIError(17,array('This Promo Code is available with minimum order amount of $'.$promo_details->min_order_amt.'.'));
		}
		if(!(in_array($promo_details->email_id, explode(",", $promo_details->send_to_users))) )
		{
			GFunctions::getAPIError(17,array('Sorry! This Promo Code is not available for you.'));
		}
		if($promo_details->usage_count <= self::PromoCodeUsageCount($user_id,$promo_code))
		{
			GFunctions::getAPIError(17,array('Sorry! You have used this Promo code already.'));
		}
	
		/// success call
		if($promo_details->discount_type == "P")
		{
			$discount	=	$promo_details->discount_percentage.'%';
			$net_amount = $total_amount - (($total_amount/100)*$promo_details->discount_percentage);
		}
		else
		{
			$discount	=	'$'.$promo_details->discount_amount;
			$net_amount = $total_amount - $promo_details->discount_amount;
		}
		
		$discount_amount = $total_amount - $net_amount;

		$responce	=	array('message'=>'Promo code has been applied successfully.','discount_type'=>$promo_details->discount_type,'total_amount'=>$total_amount,'discount'=>$discount,'net_amount'=> GFunctions::checkNULL($net_amount),'discount_amount'=> GFunctions::checkNULL($discount_amount));
		GFunctions::writeResponceAction($response);
		GFunctions::getAPISuccess($responce);
		
	}
	
	/**
	 * function to chek count of promo code used by user
	 */
	public static function PromoCodeUsageCount($user_id,$promo_code)
	{
		$session_model	=	SweatSession::find()->select(["promo_code"])
							->where(["user_id"=>$user_id,"promo_code"=>$promo_code])
							->andWhere(" status = 'S' or status = 'C' or status = 'N' or status	= 'A'")
							->all();
		$count	=	count($session_model);
		return $count;
	}
	
	/**
	 * function to chek item added in wish list or not
	 */
	public static function checkNULL($value)
	{
		if($value=="")
		{
			return "";
		}
		else
		{
			return $value;
		}
	}
	// end
	//function to get priviledges of user
	public static function priviledges()
	{
		$priviledge = array("0"=>"Read",
				"1"=>"Write",
				"2"=>"Delete");
		return $priviledge;
	}
	//function to get user Priviledges array
	public static function filterPriviledges()
	{
		$priviledge = array(""=>"All",
				"0"=>"Read",
				"1"=>"Write",
				"2"=>"Delete");
		return $priviledge;
	}
	//function to get Priviledge List
	public static function getPriviledgeList($id)
	{
		$priviledge = GFunctions::priviledges();
		$plist = '';
		if($id != '')
		{
			$ids = explode(",",$id);
			foreach($ids as $key=>$val)
			{
				$values[] = $priviledge[$val];
			}
			$plist = implode(",",$values);
		}
	
		return $plist;
	}
	
	// function to get screen names
	public static function getScreenNames()
	{
		$screens = array(
				"Users"			=>	"User Management",
				"Trainers"		=>	"Trainer management",
				"TrainersUpdate"=>	"Trainers Update Request",
				"Adminusers"	=>	"Admin Users",
				"Exams"			=>	"Manage Exams",
				"Sessions"		=>	"Session Management",
				"Cms"			=>	"CMS",
				"Setting"		=>	"Global Settings",
				"Emailtemplate"	=>	"Email Templates",
				"Contents"		=>	"Content Management",
				"Promocode"		=>	"Promo Code Management",
				"Userroles"		=>	"User Role Management"
				/*"Metavalues"=>"Meta Values Management",
				"Sitemap"=>"Add Sitemap",
				"Trackingcodes"=>"Tracking Code Management"*/
				
		);
		return $screens;
	}
	//function to get screen name
	public static function getScreenName($string,$breakAfter)
	{
		$string = array_filter(explode(",",$string));
		$final_string="";
		$i = 1;
		$screens = array(
				"Users"=>"User Management",
				"Adminusers"=>"Admin Users",
				"Cms"=>"CMS",
				"Contents"=>"Content Management",
				"Emailtemplate"=>"Email Templates",
				"Metavalues"=>"Meta Values Management",
				"Promocode"=>"Promo Code Management",
				"Setting"=>"Global Settings",
				"Sitemap"=>"Add Sitemap",
				"Trackingcodes"=>"Tracking Code Management",
				"Userroles"=>"User Role Management"
		);
		if($string !== "")
		{
			foreach($string as $str)
			{
				$final_string.= $screens[$str].",   ";
	
				if( $i % $breakAfter == 0)
				{
					$final_string.= " <br> ";
				}
				$i++;
			}
		}
		$final_string = substr($final_string, 0, strrpos($final_string, ","));
		$final_string = rtrim($final_string,",");
		return $final_string;
	}// end of function
	
	// 	function to get role details
	public static function getRoleDetails($role_id)
	{
		$role_model = UserRoles::find()->select(["privileges","screen_name"])
		->where("status = 'A' and user_role_id = $role_id")
		->one();
	
		if($role_model != NULL)
		{
			$value['privileges'] = $role_model->privileges;
			$value['screen_name'] = $role_model->screen_name;
			$value['status'] = $role_model->status;
		}
		else
			$value = '';
		return $value;
	}
	
	//function to get screen name
	public static function getAccess($controller, $action = '0')
	{
		
		if(isset(Yii::$app->user->identity->admin_id) && !empty(Yii::$app->user->identity->admin_id))
		{
			$admin_id = Yii::$app->user->identity->admin_id;
			$user_role_id = Yii::$app->user->identity->user_role_id;
			
			if($user_role_id == 0)
			{
				return true;
			}
			elseif($user_role_id !='')
			{
				$user_role_details=GFunctions::getRoleDetails($user_role_id);
				//print_r($user_role_details);exit;
				if($user_role_details!=NULL)
				{
					$user_role_priviledges=explode(',',$user_role_details['privileges']);
					$user_role_screen_name=explode(',',$user_role_details['screen_name']);
					
					if(in_array($controller,$user_role_screen_name))
						return true;
					else
						return false;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	// function to get user roles
	public static function getUserRoles($flag = NULL,$field=NULL)
	{
		$roles = array();
		
		$role_model = UserRoles::find()
		->where("status='A'")
		->all();
		
		
		if($role_model != NULL)
		{
			if($flag == 'filter')
			{
				$roles[""] = "All";
			}
			foreach($role_model as $key=>$val)
			{
				$roles[$val->$field] = $val->user_role_name." (".GFunctions::getPriviledgeList($val->privileges).")";
			}
		}
		return $roles;
	}
// Common function to accept json request
	public static function getJSONRequest()
	{
		
		if(isset($_REQUEST) || isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
			if(isset($_REQUEST)) {
				$json = $_REQUEST;
			} else {
				if(isset($GLOBALS['HTTP_RAW_POST_DATA']) == TRUE){
					$json = $GLOBALS['HTTP_RAW_POST_DATA'];
				} else {
					$json = $HTTP_RAW_POST_DATA;
				}
			}
			// This request sent by iOS
			$json = json_encode($json);
			$json = utf8_encode($json);
			$decode = json_decode($json, true);
			
			if(is_string ($decode) )
				$decode = json_decode($decode, true);
			
			return $decode;
		}
	}
	//function for giving error msg for webservices
	public static function getAPIError($error_id,$data=array(),$response = array()){
		
		require_once('errorCodeAPI.php');
		$errorText = $error[$error_id];
		$vars = array();
		for($i=0; $i<count($data); $i++){
			$vars['{'.($i+1).'}'] = $data[$i];
		}
		$errorText = strtr($errorText, $vars);
		$response_show = array("status" => "Error", "ErrorCode"=>$error_id,"Error" => $errorText);
		if(count($response)){
			foreach($response as $key=>$val){
				$response_show[$key] =  $val;
			}
		}
		
		header('Content-type: application/json');
		echo $response = json_encode($response_show);
		exit;
	}
	//function for api data filter
	public static function getAPIDataFilter($decode, $mandatoryParam){
		$return = array();
		foreach($mandatoryParam as $param){
			$paramArray = explode("|",$param);
			if(isset($decode[$paramArray[0]]) && $decode[$paramArray[0]]!="") {
				if(isset($paramArray[1]) && ($paramArray[1]=='AES' || $paramArray[1]=='base64')){
					if($paramArray[1]=='AES'){
						$aes = new AES();
						$return[$paramArray[0]] = $aes->decrypt($decode[$paramArray[0]]);
					}
					elseif ($paramArray[1]=='base64')
					{
						$return[$paramArray[0]] = base64_decode($decode[$paramArray[0]]);
					}
				} else {
					$return[$paramArray[0]] = $decode[$paramArray[0]];
				}
			} else {
				$return[$paramArray[0]] = '';
			}
		}
		return $return;
	}
	
	//function for giving success msg for webservices
	public static function getAPISuccess($response = array()){
		$response_show["status"] =  "Success";
		$response_show["data"] = [];
		if(count($response)){
			foreach($response as $key=>$val){
				$response_show["data"][$key] =  $val;
			}
		}
		header('Content-type: application/json');
		echo $response = json_encode($response_show);
		exit;
	}
	
	//function for giving success msg for webservices
	public static function uploadImage($multipart,$image_name, $FILES, $previous_photo,$folder_path,$image_name_ext = "")
	{		
		$response_array = array("status"=>"Error");
		if(empty($FILES[$image_name]["name"]))
		{
				$response_array = array("status"=>"Error", "error_code"=>50);
		}
		elseif(!in_array(strtolower(pathinfo($FILES[$image_name]["name"], PATHINFO_EXTENSION)), array("jpg","jpeg","bmp","gif","png")))
		{
			$current_file_format	=	strtolower(pathinfo($FILES[$image_name]["name"], PATHINFO_EXTENSION));
			$response_array = array("status"=>"Error", "error_code"=>51,"file_format"=>$current_file_format);
		}
		else
		{ 
			$extention = pathinfo($FILES[$image_name]["name"], PATHINFO_EXTENSION);
			$file_name=time().".".$extention;
			if($image_name_ext != "")
				$file_name=time()."_p_".$image_name_ext.".".$extention;
			
			move_uploaded_file($FILES[$image_name]["tmp_name"], yii::$app->params["image_save_path"].$folder_path.$file_name);
			if(Yii::$app->params['s3_set']=="Y")
			{
				//echo '<pre>';print_r(yii::$app->params["image_save_path"].$folder_path.$file_name);exit;
				$s3_path	=	yii::$app->params["image_save_path"].$folder_path.$file_name;
				Yii::$app->resourceManager->save(yii::$app->params["image_save_path"].$folder_path.$file_name, $s3_path);
				if($file_name!="default.png")
				{
					unlink(yii::$app->params["image_save_path"].$folder_path.$file_name);
				}				
			}
			
			if($previous_photo!="" && $previous_photo!="default.jpg")
			{
				if(Yii::$app->params['s3_set']=="Y")
				{
					if(Yii::$app->resourceManager->fileExists($folder_path.$previous_photo))
						Yii::$app->resourceManager->delete($folder_path.$previous_photo);
				}
				else
				{
					if(file_exists(yii::$app->params["image_save_path"].$folder_path.$previous_photo))
					{
						if($file_name!="default.png")
						{
							unlink(yii::$app->params["image_save_path"].$folder_path.$previous_photo);
						}
					}
				}
			}
			$response_array = array("status"=>"Success", "image_path"=>$file_name);
		}
		return $response_array;
	}
	//function for giving success msg for webservices
	public static function uploadImage1($multipart,$image_name, $FILES, $previous_photo,$folder_path,$image_name_ext = "")
	{
	
		$response_array = array("status"=>"Error");
		if(empty($FILES[$image_name]["name"]))
			$response_array = array("status"=>"Error", "error_code"=>50);
			else if(!in_array(strtolower(pathinfo($FILES[$image_name]["name"], PATHINFO_EXTENSION)), array("jpg","jpeg","bmp","gif","png")))
				$response_array = array("status"=>"Error", "error_code"=>51);
				else
				{
					$extention = pathinfo($FILES[$image_name]["name"], PATHINFO_EXTENSION);
					$file_name=time().".".$extention;
					if($image_name_ext != "")
						$file_name=time()."_p_".$image_name_ext.".".$extention;
							
						move_uploaded_file($FILES[$image_name]["tmp_name"], yii::$app->params["s3_path"].$folder_path.$file_name);
						if(Yii::$app->params['s3_set']=="Y")
						{
							Yii::$app->resourceManager->save($folder_path.$file_name, $file_name);
							unlink(yii::$app->params["s3_path"].$folder_path.$file_name);
						}
							
						if($previous_photo!="" && $previous_photo!="default.jpg")
						{
							if(Yii::$app->params['s3_set']=="Y")
							{
								if(Yii::$app->resourceManager->fileExists($folder_path.$previous_photo))
									Yii::$app->resourceManager->delete($folder_path.$previous_photo);
							}
							else
							{
								if(file_exists(yii::$app->params["image_save_path"].$folder_path.$previous_photo))
									unlink(yii::$app->params["image_save_path"].$folder_path.$previous_photo);
							}
						}
						$response_array = array("status"=>"Success", "image_path"=>$file_name);
				}
				return $response_array;
	}
	/**
	 * Function to send SMS
	 * @param unknown $to
	 * @param unknown $body
	 */
	public static function SendSMS($to,$body)
	{
		$sid = Yii::$app->params['twilio_sid'];
		$token = Yii::$app->params['twilio_token'];
		$from = Yii::$app->params['twilio_from_number'];
		$client = new Client($sid, $token);
		try{
			if($client->messages->create( $to, array('from' => $from,'body' =>$body ) ))
				return 1;
			else 
				return false;
			}
		catch (\Twilio\Exceptions\RestException $e) {
				$msg	=	explode("]",$e->getMessage());
                return $msg[1];
        }
	}
	/**
	 * Function to get Driving Distance
	 * @param unknown $lat1
	 * @param unknown $lat2
	 * @param unknown $long1
	 * @param unknown $long2
	 */
	public static function getDrivingDistance($lat1, $lat2, $long1, $long2)
	{
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL&key=AIzaSyAmcrmNMLlsBy5lsmcwjG43ny5B5Q5Wr2k";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		if($response_a['rows'][0]['elements'][0]['status'] != 'NOT_FOUND' && $response_a['rows'][0]['elements'][0]['status'] != 'ZERO_RESULTS')
		{
			$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
			$time = $response_a['rows'][0]['elements'][0]['duration']['value'];
			return array('distance' => $dist, 'time' => $time);
		}
		else
		{
			return array('distance' => -1, 'time' => -1);
		}
	}
	
	/**
	 * Function to get closest number in array
	 * @param unknown $number
	 */
	public static function getClosestNumber($number)
	{
		$array		=	[1,1.5,2,2.5,3,3.5,4,4.5,5];
		$current	=	$array[0];
		foreach($array as $val)
		{
			if(abs($number-$val) < abs($number-$current))
			{
				$current	=	$val;
			}
		}
	
		return $current;
	}
	/**
	 * Function to get converted weight
	 * @param unknown $weight
	 * @param unknown $weight_dimension
	 */
	public static function getWeightConversion($weight,$weight_dimension)
	{
		$one_kg = 1;
		if($weight_dimension=="lbs")
		{
			$one_kg = 2.20462;
		}
		$weight = ($weight*(1/$one_kg));
		
		return $weight;
	}
	/**
	 * Function to get converted height
	 * @param unknown $height
	 * @param unknown $height_dimension
	 */
	public static function getHeightConversion($height,$height_dimension)
	{
		$one_meter = 1;
		if ($height_dimension=="cm")
		{
			$one_meter = 100;
		}
		elseif($height_dimension=="ft")
		{
			$one_meter = 3.28084;
		}
		elseif($height_dimension=="inch"){
			$one_meter = 39.3701;
		}
		$height = ($height/$one_meter);
		
		return $height;
	}

	/**
	 * Function to get session booking for who
	 * @param unknown $booking_for
	 */
	public static function getSessionBookingFor($booking_for)
	{
		if($booking_for=="S")
		{
			$booking_for_name = 'Self';
		}
		else{
			$booking_for_name = 'Friend';
		}
	
		return $booking_for_name;
	}
	
	/**
	 * Function to get session category name
	 * @param unknown $booking_for
	 */
	public static function getSessionCategory($session_category)
	{
		if($session_category=="O")
		{
			$category_name = 'One-on-One';
		}
		else{
			$category_name = 'Group';
		}
	
		return $category_name;
	}
	
	/**
	 * Function to get global key name from trainer level
	 * @param unknown $trainer_level
	 */
	public static function getTrainerLevelGlobalKey($trainer_level)
	{
		if($trainer_level=="E")
		{
			$global_key		=	"TRAINER_ELITE_LEVEL_PERCENT";
		}
		elseif($trainer_level=="D1")
		{
			$global_key		=	"TRAINER_DIVISION_1_LEVEL_PERCENT";
		}
		elseif($trainer_level=="D2")
		{
			$global_key		=	"TRAINER_DIVISION_2_LEVEL_PERCENT";
		}
		else
		{
			$global_key		=	"TRAINER_DIVISION_3_LEVEL_PERCENT";
		}
		return $global_key;
	}

	/**
	 * Function to get trainer levels list
	 */
	public static function getTrainerLevelCodelist()
	{
		$code_list	=	array(	"E"	=>	"Elite-CPT",
								"D1"=>	"D1-CPT",
								"D2"=>	"D2-CPT",
								"D3"=>	"D3-CPT"
							);
		
		return $code_list;
	}
	
	/**
	 * Function to get trainer level code from trainer level
	 * @param unknown $trainer_level
	 */
	public static function getTrainerLevelCodeName($trainer_level)
	{
		if($trainer_level=="E")
		{
			$level_code		=	"Elite-CPT";
		}
		elseif($trainer_level=="D1")
		{
			$level_code		=	"D1-CPT";
		}
		elseif($trainer_level=="D2")
		{
			$level_code		=	"D2-CPT";
		}
		else
		{
			$level_code		=	"D3-CPT";
		}
		return $level_code;
	}
	
	/**
	 * Function to get group size
	 * @param unknown $group_size
	 */
	public static function getGroupSize($group_size)
	{
		if($group_size=="2")
		{
			$size = '2-4';
		}
		elseif($group_size=="5")
		{
			$size = '5-7';
		}
		elseif($group_size=="8")
		{
			$size = '8-10';
		}
		else{
			$size = '1';
		}
	
		return $size;
	}
	
	/**
	 * Function to get session fitness style name
	 * @param unknown $style
	 */
	public static function getSessionFitnessStyle($style)
	{
		$style_name	=	"";
		
		if($style=="M"){
			$style_name = "Mobility";
		}
		elseif($style=="H"){
			$style_name = "H.I.I.T";
		}
		elseif($style=="S"){
			$style_name = "Strength";
		}
		elseif($style=="A"){
			$style_name = "Athlete";
		}
	
		return $style_name;
	}
	
	/**
	 * Function to get preferred trainer gender name
	 * @param unknown $booking_for
	 */
	public static function getPreferredTrainer($trainer_gender)
	{
		if($trainer_gender=="M")
		{
			$gender_name = 'Male';
		}
		elseif($trainer_gender=="M"){
			$gender_name = 'Female';
		}
		else{
			$gender_name=	'Either';
		}
	
		return $gender_name;
	}
	
	/**
	 * Function to get session status
	 * @param unknown $status
	 */
	public static function getSessionStatus($status)
	{
		if($status=="N"){
			$status_name = "Pending";
		}
		elseif ($status=="A"){
			$status_name = "Confirmed";
		}
		elseif ($status=="S"){
			$status_name = "Started";
		}
		elseif ($status=="C"){
			$status_name = "Completed";						
		}
		elseif ($status=="R"){
			$status_name = "Rejected";
		}
		elseif ($status=="NSC"){
			$status_name = "NoShowCustomer";
		}
		elseif ($status=="NST"){
			$status_name = "NoShowTrainer";
		}
		elseif ($status=="CBU"){
			$status_name = "CancelledByCustomer";
		}
		elseif ($status=="CBT"){
			$status_name = "CancelledByTrainer";
		}
		elseif ($status=="RT"){
			$status_name = "Rated";
		}
		else{
			$status_name="";
		}
	
		return $status_name;
	}

	/**
	 * Function to get time difference between two dates
	 * @param unknown $to_date
	 * @param unknown $from_date
	 */
	public static function getTimeDifference($to_date,$from_date)
	{
		//$current_date	=	date('Y-m-d H:i:s');
		$to_time 		= 	strtotime($to_date);
		$from_time 		= 	strtotime($from_date);
		$time_diff 		= 	round(abs($to_time - $from_time) / 60,2);
	
		return $time_diff;
	}
	
	/**
	 *
	 * Function to Create Trainers Managed Bank account on stripe
	 * @param unknown $trainer_id
	 */
	public static function createStripeAccount($trainer_id)
	{	
		$trainer_id 	=	$trainer_id;
		
		if($trainer_id!=NULL)
		{
			$trainer_model	=	Trainers::find()
								->select(["trainer_id","trainer_stripe_account_id","email_id","first_name","last_name","address_line1","city","state","country","zip","account_number","routing_number"])
								->where(["trainer_id"=>$trainer_id])
								->one();
			
			if(!empty($trainer_model))
			{
				if($trainer_model->trainer_stripe_account_id==NULL || $trainer_model->trainer_stripe_account_id=="" || $trainer_model->trainer_stripe_account_id==0)
				{
					$dob	= 	strtotime($trainer_model->dob);
					$day	= 	date('d',$dob);
					$month	= 	date('m',$dob);
					$year	=	date('Y',$dob);
					
					try
					{
						//*****Initialize API*****/
						\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);
		
						//*****Creating Trainers account on Myos Stripe account*****/
						$stripe_account = Account::create(array("email"				=>	$trainer_model->email_id,
																"managed"			=> 	true,
																"country"			=>	$trainer_model->country,
																"default_currency"	=>	"usd",
																"external_account" 	=> 	array(	"object" 				=> 	"bank_account",
																								"country" 				=> 	$trainer_model->country,
																								"currency" 				=> 	"usd",
																								"account_holder_name" 	=> 	$trainer_model->first_name.' '.$trainer_model->last_name,
																								"account_holder_type" 	=> 	"individual",
																								"routing_number" 		=> 	$trainer_model->routing_number,
																								"account_number" 		=> 	$trainer_model->account_number,
																						),
																"legal_entity"		=>	array(	"type"					=>	"individual",
																								"first_name"			=>	$trainer_model->first_name,
																								"last_name"				=>	$trainer_model->last_name,
																								"address"				=>	array(	"line1"			=>	$trainer_model->address_line1,
																																	"city"			=>	$trainer_model->city,
																																	"state"			=>	$trainer_model->state,
																																	"country"		=>	$trainer_model->country,
																																	"postal_code"	=>	$trainer_model->zip
																															),
																								"dob"					=>	array("day"=>$day,"month"=>$month,"year"=>$year)
																						),
																"tos_acceptance"	=>	array("date"=>time(),"ip"=>$_SERVER['REMOTE_ADDR'])
															));
		
						return	$stripe_account['id'];
						
						
					}
					catch(\Stripe\Error\Card $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (\Stripe\Error\RateLimit $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (\Stripe\Error\InvalidRequest $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (\Stripe\Error\Authentication $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (\Stripe\Error\ApiConnection $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (\Stripe\Error\Base $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
					catch (Exception $e)
					{
						$data['error']	= $e->getMessage();
						return $data;
						//GFunctions::getAPIError(18,array($e->getMessage()));
					}
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}			
		}
		else{
			return 0;
		}
			
		//*****View All Bank Accounts on this secrete Token*****/
		//\Stripe\Stripe::setApiKey('sk_test_OKTKMJ4sSwTXJbIgnpFpdYAH');
		//$list_all_accounts	=	Account::all(array("limit" => 3));
		/* 			$stripe_account = Account::create(array("email"				=>	"yadavnileshya@gmail.com",
		 "managed"			=> 	true,
		 "country"			=>	"US",
		 "default_currency"	=>	"usd",
		 "external_account" 	=> 	array(	"object" 				=> 	"bank_account",
		 "country" 				=> 	"US",
		 "currency" 				=> 	"usd",
		 "account_holder_name" 	=> 	"Nilesh Yadav",
		 "account_holder_type" 	=> 	"individual",
		 "routing_number" 		=> 	"110000000",
		 "account_number" 		=> 	"000123456789",
		 ),
		 "legal_entity"		=>	array(	"type"				=>	"individual",
		 "first_name"		=>	"Nilesh",
		 "last_name"			=>	"Yadav",
		 "address"			=>	array(	"country"		=>	"US",
		 "line1"			=>	"1234 Main Street",
		 "postal_code"	=>	"94111",
		 "city"			=>	"San Francisco",
		 "state"			=>	"CA"
		 ),
		 "dob"				=>	array("day"=>"12","month"=>"06","year"=>"1990"),
		 "ssn_last_4"		=>	"6789",
		 "personal_id_number"=>	"123456789"
		 ),
		 "tos_acceptance"	=>	array("date"=>"1486625702","ip"=>"202.189.247.61")
	
		 )); */
	}
	
	public static function updateStripeAccount($account_id,$routing_number,$account_number,$country)
	{
		try
		{
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Connect to stripe account
				
			$account = \Stripe\Account::retrieve($account_id);// Retrive account from stripe
			
			$account->external_account	=	array(	"object" 				=> 	"bank_account",
													"country" 				=> 	$country,
													"currency" 				=> 	"usd",
													"routing_number" 		=> 	$routing_number,
													"account_number" 		=> 	$account_number,
												);
			
			$update	= $account->save();
			//$account = \Stripe\Account::retrieve($update->id);
			//print_r($account);exit;
			return $update->id;
		}
		catch(\Stripe\Error\Card $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
	
	public static function updateStripeAccountAdmin($account_id,$routing_number,$account_number,$country)
	{
		try
		{
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Connect to stripe account
	
			$account = \Stripe\Account::retrieve($account_id);// Retrive account from stripe
				
			$account->external_account	=	array(	"object" 				=> 	"bank_account",
					"country" 				=> 	$country,
					"currency" 				=> 	"usd",
					"routing_number" 		=> 	$routing_number,
					"account_number" 		=> 	$account_number,
			);
				
			$update	= $account->save();
			//$account = \Stripe\Account::retrieve($update->id);
			//print_r($account);exit;
			return $update->id;
		}
		catch(\Stripe\Error\Card $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			$data['error']	= $e->getMessage();
			return $data;
			//GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
	
	/**
	 * Function to Create charge on stripe but not capture 
	 * @param unknown $amount
	 * @param unknown $customer_account_id
	 * @param unknown $user_email_id
	 * @param unknown $trainer_account_id
	 * @param unknown $application_fee_cent
	 */	
	public static function createCharge($amount,$card_id,$customer_account_id,$user_email_id,$trainer_account_id,$application_fee_cent,$capture)
	{
		try
		{
			if($capture==1)
			{
				$capture_status	=	true;
			}
			else{
				$capture_status	=	false;
			}
			/*********************************test server api details************************************/
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Server stripe Key
			$payment_details	=	array(	"amount" 			=>	$amount,
											"currency" 			=> 	"usd",
											"capture"			=>	$capture_status,
											"customer" 			=> 	$customer_account_id, // obtained with Stripe.js
											"description" 		=> 	"Charge for ".$user_email_id,
											"destination"		=>	array("account" => $trainer_account_id),
											"application_fee"	=>	$application_fee_cent
										);
			if($card_id!="")
			{
				$payment_details['source']	=	$card_id;
			}
			return \Stripe\Charge::create($payment_details);
			
			
			
/* 			\Stripe\Stripe::setApiKey("sk_test_sz17qQLvc8HkUVICXlp0sBHY");	//localhost test stripe key
			//*****Creating Token*****
	 		$token = Token::create(array("card"		=> 	array(
										"number" 	=> 	"4242424242424242",
										"exp_month" => 	2,
										"exp_year" 	=> 	2018,
										"cvc"		=> 	"314"
										)
									));
		
			 $customer = Customer::create(array(	'email'	=> 'nilu.4179@gmail.com',
												'source'=> $token
										));
			//print_r($customer);exit; 
			return \Stripe\Charge::create(array("amount" 			=>	$amount,//amount in cent
												"currency" 			=> 	"usd",
												"capture"			=>	false,
												"customer" 			=>  $customer->id, // obtained with Stripe.js
												"description" 		=> 	"Charge for ".$user_email_id,
												"destination"		=>	$trainer_account_id,
												"application_fee"	=>	$application_fee_cent //application fee in cent
										)); */
		}
		catch(\Stripe\Error\Card $e)
		{
			// Since it's a decline, \Stripe\Error\Card will be caught
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			// Too many requests made to the API too quickly
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			// Invalid parameters were supplied to Stripe's API
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			// Network communication with Stripe failed
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			// Display a very generic error to the user, and maybe send
			// yourself an email
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			// Something else happened, completely unrelated to Stripe
			GFunctions::getAPIError(18,array($e->getMessage()));
		}		
	}
	
	/**
	 * function to update Stripe charge
	 * @param unknown $charge_id
	 */
	public static function updateCharge($charge_id,$amount_cent,$application_fee_cent)
	{
		try
		{
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Connect to stripe account
				
			$ch 					= 	\Stripe\Charge::retrieve($charge_id);// Retrive charge from stripe
			//print_r($ch);exit;
			$ch->amount				=	$amount_cent;
			$ch->application_fee	=	$application_fee_cent;
			
			//print_r($ch);exit;
			return $ch->update($charge_id,array("amount"=>$amount_cent,"application_fee"=>$application_fee_cent));// save charge or update transaction
		}
		catch(\Stripe\Error\Card $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
	
	/**
	 * function to capture Stripe charge
	 * @param unknown $charge_id
	 */
	public static function captureCharge($charge_id)
	{
		try
		{
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Connect to stripe account			 
			
			$ch = \Stripe\Charge::retrieve($charge_id);// Retrive charge from stripe	
			return $ch->capture();// capture charge or complete transaction
		}
		catch(\Stripe\Error\Card $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
	/**
	 * function to Refund Stripe charge if trainer not available for session
	 * @param unknown $charge_id
	 * @param unknown $amount
	 */
	public static function refundCharge($charge_id,$amount)
	{
		try
		{
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Connect to stripe account			 
			
			return \Stripe\Refund::create(array( "charge"	=>	$charge_id,
												"amount"	=>	$amount,
												"reverse_transfer" => true,
												"refund_application_fee"=> true
										));// Refund charge from stripe	
			
		}
		catch(\Stripe\Error\Card $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}	
	
	
	public static function createUserAccount($user_email_id)
	{
		try
		{
			/*********************************test server api details************************************/
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Server stripe Key
			
				//*****Creating Token*****
				$token = Token::create(array("card"		=> 	array(
											"number" 	=> 	"4242424242424242",
											"exp_month" => 	2,
											"exp_year" 	=> 	2018,
											"cvc"		=> 	"314"
											)
				));
	
				$customer = Customer::create(array(	'email'	=> $user_email_id,
													'source'=> $token
													));
				
				return $customer->id;
		}
		catch(\Stripe\Error\Card $e)
		{
			// Since it's a decline, \Stripe\Error\Card will be caught
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			// Too many requests made to the API too quickly
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			// Invalid parameters were supplied to Stripe's API
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			// Network communication with Stripe failed
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			// Display a very generic error to the user, and maybe send
			// yourself an email
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			// Something else happened, completely unrelated to Stripe
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
	
	public static function retriveCardId($customer_id)
	{
		try
		{
			/*********************************test server api details************************************/
			\Stripe\Stripe::setApiKey(\Yii::$app->params['stripe_key']);  // Server stripe Key
				
			//*****Creating Token*****
			/* $token = Token::create(array(	"card"		=> 	array(
											"number" 	=> 	"4242424242424242",
											"exp_month" => 	2,
											"exp_year" 	=> 	2018,
											"cvc"		=> 	"314"
										)
								));
	 */
			$customer = \Stripe\Customer::retrieve($customer_id);
			//print_r($customer->sources->data[0]->id);exit;
			return $customer->sources->data[0]->id;
			//$customer_card->sources->create(array("source" => $token));

	
			//return $customer_card->id;
		}
		catch(\Stripe\Error\Card $e)
		{
			// Since it's a decline, \Stripe\Error\Card will be caught
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\RateLimit $e)
		{
			// Too many requests made to the API too quickly
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\InvalidRequest $e)
		{
			// Invalid parameters were supplied to Stripe's API
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Authentication $e)
		{
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\ApiConnection $e)
		{
			// Network communication with Stripe failed
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (\Stripe\Error\Base $e)
		{
			// Display a very generic error to the user, and maybe send
			// yourself an email
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
		catch (Exception $e)
		{
			// Something else happened, completely unrelated to Stripe
			GFunctions::getAPIError(18,array($e->getMessage()));
		}
	}
}
?>
