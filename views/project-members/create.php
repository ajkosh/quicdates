<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectMembers */

$this->title = Yii::t('app', 'Create Project Members');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project'), 'url' => ['projects/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-members-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
