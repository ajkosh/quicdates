<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Usertype;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

     <?php $form = ActiveForm::begin([
     'options' => ['class' => 'bv-form'],
     'id'      => 'user-form',
     'action'=>['create','id'=>$_GET['id']],
     // 'enableAjaxValidation'   => true,
     // 'enableClientValidation' => false,
     'fieldConfig' => [
     'template' => "{label}\n<div class=\"col-xs-8\">{input}{hint}{error}</div>",
     'labelOptions' => ['class' => 'col-sm-4 control-label'],
    ],
     ]); ?>
    <div class="row">
        <div class="col-md-12">
             <div class="col-md-6">
        <?php //echo $form->field($model, 'usertype_id')->widget(Select2::classname(), [
            // 'data' => Usertype::getDropDownData(),
            // 'language' => 'en',
            //   'options' => ['placeholder' => Yii::t('app','Select Role...')],
            //     'pluginOptions' => [
            //      'allowClear' => false
            //     ],
                //'disabled' => !$model->isNewRecord,
 //   ]);?>
  <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => User::getDropDownData(),
            'language' => 'en',
              'options' => ['placeholder' => Yii::t('app','Select Members...'),'multiple'=>true],
                'pluginOptions' => [
                 'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
    ]);?>
    

</div>
 <div class="col-md-6">
      

</div>
</div>
 
</div>
</div>
  <div class="form-group btn-group">
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

         <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['projects/index'], ['class'=>'btn btn-danger']); ?>
         <?php if($model->isNewRecord): ?>
         
         <button type="reset" class="btn btn-info">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
         </button>
         <?php endif ?>
      </div>
   

    <?php ActiveForm::end(); ?>

</div>
