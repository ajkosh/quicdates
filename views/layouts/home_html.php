<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?= Html::csrfMetaTags() ?>
		<title><?php echo Yii::$app->params['site_title'];?></title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="Roman Kirichik">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<?php $this->head();   ?>
        <link rel="shortcut icon" href="<?php echo Yii::$app->params['live_path'];?>/web/front/images/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo Yii::$app->params['live_path'];?>/web/front/images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::$app->params['live_path'];?>/web/front/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::$app->params['live_path'];?>/web/front/images/apple-touch-icon-114x114.png">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/style.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/custom.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/style-responsive.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/animate.min.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/vertical-rhythm.min.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->params['live_path'];?>/web/front/css/blue.css">
</head>	
<body class="appear-animate">
        <!-- Page Loader -->        
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <div class="page" id="top">
            
            <!-- Navigation panel -->
            <nav class="main-nav stick-fixed">
                <div class="full-wrapper relative clearfix">
                    <!-- Logo ( * your text or image into link tag *) -->
                    <div class="nav-logo-wrap local-scroll">
                        <a href="#top" class="logo">
                            <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/logo-dark-blue.png" alt="" />
                        </a>
                    </div>
                    <div class="mobile-nav">
                        <i class="fa fa-bars"></i>
                    </div>
                    <!-- Main Menu -->
                    <div class="inner-nav desktop-nav">
                        <ul class="clearlist scroll-nav local-scroll">
                            <li class="active"><a href="#home"><span>Home</span></a></li>
                            <li><a href="#about"><span>About</span></a></li>
                            <li><a href="#services"><span>Why?</span></a></li>
                            <li><a href="#portfolio"><span>How It Work's</span></a></li>
                            <li><a href="#testimonial"><span>Testimonial</span></a></li>
                            <li><a href="#contact"><span>Contact</span></a></li>
                        </ul>
                    </div>
                    <!-- End Main Menu -->
                </div>
            </nav>
            <!-- End Navigation panel -->
            
             <?php echo $content; ?>
            
            
            <!-- Foter -->
            <footer class="small-section footer pb-60">
                <div class="container">
                    
                    <!-- Social Links -->
                    <div class="footer-social-links mb-60 mb-xs-40">
                        <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
                        <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <!-- End Social Links -->  
                    
                    <!-- Footer Text -->
                    <div class="footer-text">
                        
                        <!-- Copyright -->
                        <div class="footer-copy">
                            <a href="#" target="_blank">&copy; quipdates <?php echo date("Y")?></a>
                        </div>
                        <!-- End Copyright -->
                        
                    </div>
                    <!-- End Footer Text --> 
                    
                 </div>
                 
                 
                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
                 </div>
                 <!-- End Top Link -->
                 
            </footer>
            <!-- End Foter -->
        
        
        </div>
        <!-- End Page Wrap -->


        <!-- JS -->
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/SmoothScroll.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.localScroll.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.viewport.mini.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.countTo.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.parallax-1.1.3.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.simple-text-rotator.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/all.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/contact-form.js"></script>
        <script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/jquery.ajaxchimp.min.js"></script>        
        <!--[if lt IE 10]><script type="text/javascript" src="<?php echo Yii::$app->params['live_path'];?>/web/front/js/placeholder.js"></script><![endif]-->
</body>
</html>
<?php $this->endPage() ?>