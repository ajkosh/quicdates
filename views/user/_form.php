<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usertype;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

     <?php $form = ActiveForm::begin([
     'options' => ['class' => 'bv-form'],
     'id'      => 'user-form',
     // 'enableAjaxValidation'   => true,
     // 'enableClientValidation' => false,
     'fieldConfig' => [
     'template' => "{label}\n<div class=\"col-xs-8\">{input}{hint}{error}</div>",
     'labelOptions' => ['class' => 'col-sm-4 control-label'],
    ],
     ]); ?>
    <div class="row">
        <div class="col-md-12">
             <div class="col-md-6">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
</div>
 <div class="col-md-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'user_type')->widget(Select2::classname(), [
            'data' => UserType::getDropDownData(),
            'language' => 'en',
              'options' => ['placeholder' => Yii::t('app','Select Usertype...')],
                'pluginOptions' => [
                 'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
    ]);?>
</div>
</div>
 
</div>
</div>
  <div class="form-group btn-group">
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

         <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['user/index'], ['class'=>'btn btn-danger']); ?>
         <?php if($model->isNewRecord): ?>
         
         <button type="reset" class="btn btn-info">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
         </button>
         <?php endif ?>
      </div>
   

    <?php ActiveForm::end(); ?>

</div>
