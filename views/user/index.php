<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\widgets\Select2;
use app\models\Usertype;



/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php Pjax::begin(); ?>    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'id',
            // [
            //   'label'=>'Type',
            //   'attribute'=>'types.title',
            //   'filter' => Select2::widget([
            //       'model' => $searchModel,
            //       'attribute' => 'type_id',
            //       'data' => Type::getDropDownData(),
            //       //'initValueText' => $searchModel->citizenship,
            //       'options' => [
            //           'placeholder' => 'Select Type...'
            //       ]
            //   ]),
            //    'headerOptions' => ['style' => 'width:20%'],
            // ],
          
            [
              'attribute'=>'username',
               'headerOptions' => ['style' => 'width:20%'],
            ],
            [
              'attribute'=> 'email',
               'headerOptions' => ['style' => 'width:40%'],
            ],
           
            // 'password_hash',
            // 'auth_key',
            // 'confirmed_at',
            // 'blocked_at',
            // 'registration_ip',
             [
              'label'=>'User Type',
              'attribute'=>'usertypes.title',
              'filter' => Select2::widget([
                  'model' => $searchModel,
                  'attribute' => 'user_type',
                  'data' => Usertype::getDropDownData(),
                  //'initValueText' => $searchModel->citizenship,
                  'options' => [
                      'placeholder' => 'Select Usertype...'
                  ]
              ]),
               'headerOptions' => ['style' => 'width:20%'],
            ],
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'status',
            // 'is_deleted',

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{update}&nbsp{view}',
                    'buttons' => [
                    'login' => function ($url, $model) {
                                  return Html::a('Login As', $url, [
                                    'title' => Yii::t('app', 'Login As '.$model->email.' '),'class'=>'btn btn-xs btn-primary'
                                 ]);
                              }, 
                  ],

                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['user/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['user/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['user/delete','id'=>$hash]);
                         }elseif($action==='login'){
                            return Url::to(['user/login-as','key'=>$hash]);
                         } 
                    },
            ],

        ],

         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> User </h3>',
            'type'=>'primary',
             'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'User',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
          ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

<?php Pjax::end(); ?>
  
</div>
