<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usertype */

$this->title = 'Update Usertype: ' . $model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Usertypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name.' '.$model->last_name, 'url' => ['view', 'id' => Yii::$app->encryptor->encrypt($model->id)]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usertype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
     
    ]) ?>

</div>
