	
<!-- Home Section -->
            <section class="home-section bg-dark-alfa-30 parallax-3" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/section-bg-7.jpg" id="home">
                <div class="js-height-full relative">
                    
                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">
                            <div class="mt-20 mt-xs-0 mb-10">
                                <a href="https://www.youtube.com/watch?reload=9&v=T5oS5DHtCt0" class="big-icon-link lightbox-gallery-1 mfp-iframe"><span class="big-icon"><i class="fa fa-play"></i></span></a>
                            </div>                        
                            <h1 class="hs-line-13 mb-50 mb-xs-20">
                                An Intuitive & User-Friendly App<br />For Project Managers & Leaders
                            </h1>
                        </div>
                    </div>
                    <!-- End Hero Content -->
                    
                    <!-- Scroll Down -->
                    <div class="local-scroll">
                        <a href="#about" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
                    </div>
                    <!-- End Scroll Down -->
                    
                </div>
            </section>
            <!-- End Home Section -->            
            
            <!-- Promo Section -->
            <section class="split-section bg-gray">
                <div class="clearfix relative">
                    
                    <!-- Section Headings -->
                    <div class="split-section-headings left">
                        <div class="ssh-table">
                            <div class="ssh-cell page-section bg-scroll" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/split-section-1.jpg"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Section Content -->
                    <div class="split-section-content right small-section bg-gray-lighter">
                        
                        <div class="split-section-wrapper">
                            
                            <h1 class="section-title">We believe in adding value to the workplace</h1>
                            
                            <h2 class="section-heading uppercase strong">Be free in your creativity</h2>
                            
                            <div class="section-text mb-60">
                                That's why we are committed to building comprehensive digital solutions for entrepreneurs and managers that help them achieve the deliverables 
                            </div>
                            
                            <div class="row alt-features-grid">
                                
                                <!-- Features Item -->
                                <div class="col-sm-4 wow fadeInRight" data-wow-delay="0.1s">
                                    <div class="alt-features-item align-center">
                                        <div class="alt-features-icon color">
                                            <span>
                                                <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/Seamless_collaboration.png" alt="">
                                            </span>
                                        </div>
                                        <h3 class="alt-features-title">Seamless collaboration</h3>
                                    </div>
                                </div>
                                <!-- End Features Item -->
                                
                                <!-- Features Item -->
                                <div class="col-sm-4 wow fadeInRight" data-wow-delay="0.2s">
                                    <div class="alt-features-item align-center">
                                        <div class="alt-features-icon color">
                                            <span>
                                                <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/Effortless-Project-Management.png" alt="">
                                            </span>
                                        </div>
                                        <h3 class="alt-features-title"> Effortless Project Management</h3>
                                    </div>
                                </div>
                                <!-- End Features Item -->
                                
                                <!-- Features Item -->
                                <div class="col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                                    <div class="alt-features-item align-center">
                                        <div class="alt-features-icon color">
                                            <span>
                                                <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/Timely-Delivery.png" alt="">
                                            </span>
                                        </div>
                                        <h3 class="alt-features-title"> Timely Delivery</h3>
                                    </div>
                                </div>
                                <!-- End Features Item -->
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- End Section Content -->
                    
                </div>
            </section>
            <!-- End Promo Section -->            
            
            <!-- About Section -->
            <section class="page-section" id="about">
                <div class="container relative">
                    
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            <h2 class="section-heading">
                                About Us
                            </h2>
                            <!-- End Section Titles -->
                            
                            <div class="section-line mb-50 mb-xs-30"></div>
                            
                            <!-- Section Text -->
                            <div class="section-text mb-80 mb-xs-50">
                                <p>
                                    In this digital era, entrepreneurs are embracing advanced tech-driven solutions and apps for streamlining all their departments as well as complex processes. The domain of project management isn't an exception to this notion. But to be in the shoes of project leaders is a daunting task. They have to go through a hell lot of hurdles to successfully accomplish a project and most importantly satisfy a client. 
                                </p>
                                <p>
                                    Quipdates is a one-stop solution for making the lives of leaders and project managers more convenient. It can function seamlessly on the web and mobile (iOS) platforms. It offers a wide range of features allowing project mentors to keep an eye on the status of multiple projects and manage resources, sprints, and other project related issues.
                                </p>

                            </div>
                            <!-- End Section Text -->
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- End About Section -->            
            
            <!-- Skills Section -->
            <section class="split-section bg-gray">
                <div class="clearfix relative">
                    
                    <!-- Section Headings -->
                    <div class="split-section-headings right">
                        <div class="ssh-table">
                            <div class="ssh-cell page-section bg-scroll" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/split-section-2.jpg"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Section Content -->
                    <div class="split-section-content small-section bg-gray-lighter">
                        
                        <div class="split-section-wrapper left">
                            
                            <h1 class="section-title">Entrust your project to digital gurus</h1>
                            
                            <h2 class="section-heading uppercase strong">We&rsquo;re the best in&nbsp;this field</h2>
                            
                            <div class="section-text mb-60">
                                Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis. Integer erat libero, molestie et dapibus ac, egestas nec nulla.  
                            </div>
                            
                            <div>
                                
                                <!-- Bar Item -->
                                <div class="progress tpl-progress-alt progress-color">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                                        Project Management <span class="right">90%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                
                                <!-- Bar Item -->
                                <div class="progress tpl-progress-alt progress-color">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                      Work Tracking <span class="right">90%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                
                                <!-- Bar Item -->
                                <div class="progress tpl-progress-alt progress-color">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                                      Institutional Integrity   <span class="right">80%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                
                                <!-- Bar Item -->
                                <div class="progress tpl-progress-alt progress-color">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                                        Resourse Management <span class="right">90%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                
                            </div>
                            
                            
                        </div>
                        
                    </div>
                    <!-- End Section Content -->
                    
                </div>
            </section>
            <!-- End Skills Section -->            
            
            <!-- Services Section -->
            <section class="page-section" id="services">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <!-- Section Titles -->
                            <h1 class="section-title mb-10">
                                Why Quipdates?
                            </h1>
                            <h2 class="section-heading">
                                Curate, Collaborate, & Command Over Multiple Projects With Quipdates
                            </h2>
                            <!-- End Section Titles -->
                            
                            <div class="section-line mb-50 mb-xs-30"></div>
                            
                            <!-- Section Text -->
                            <div class="section-text mb-80 mb-xs-50">
                                Our goal is to empower project leaders and managers with all the abilities and powers they need to monitor the progress of their current projects and efficiently manage available resources. We have built this digital solution with the sole intent to make managers' lives hassle-free by paying undivided attention to the following four aspects:
                            </div>
                            <!-- End Section Text -->
                            
                        </div>
                    </div>
                    
                    
                    <!-- Services Grid -->
                    <div class="row multi-columns-row service-grid">
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-circle-compass"></span>
                                </div>
                                <h3 class="service-title">Usability</h3>
                                The app is a perfect fit for project managers and leaders, as it's customized to fulfil their specific requirements and letting them tailor workflows and keep a track of different sprints, resources, and issues associated with multiple on-going projects.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-presentation"></span>
                                </div>
                                <h3 class="service-title">Functionality</h3>
                                Quipdates is power-packed with several impressive features for the admin as well as the project managers. Every single function and feature is built to serve it's intended purpose efficiently.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-linegraph"></span>
                                </div>
                                <h3 class="service-title">Flexibility</h3>
                                One of the most striking features of this app is that it's extremely flexible. It's built with a notion in mind that it will be operated in a dynamic environment. This is why we didn't stick too rigidly to an initial product roadmap which paved the way for space required by Quipdates to evolve.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-browser"></span>
                                </div>
                                <h3 class="service-title">Scalability</h3>
                                The coders have crafted this app with the sole motive to make it highly scalable. Therefore, incorporating new features isn't a nightmare anymore. It has the potential to handle a sudden surge in the number of users without getting stuck into a gridlock.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-tools"></span>
                                </div>
                                <h3 class="service-title">Development</h3>
                                Fusce aliquet quam eget neque ultrices elementum. Nulla posuere 
                                felis id arcu blandit sagittis sit amet lorem vulputate risus.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                        <!-- Service Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="service-item">
                                <div class="service-icon color">
                                    <span class="icon-anchor"></span>
                                </div>
                                <h3 class="service-title">Support</h3>
                                Pulvinar vitae neque et porttitor. Integer non dapibus diam, 
                                ac eleifend lectus. Praesent sed nisi eleifend, fermentum orci sit amet.
                            </div>
                        </div>
                        <!-- End Service Item -->
                        
                    </div>
                    <!-- End Services Grid -->
                    <!-- Portfolio Section -->
                        <div class="container why">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 align-center">
                                    
                                    <!-- Section Titles -->
                                    <h1 class="section-title mb-10">
                                        Our best works will tell all than us
                                    </h1>
                                    <h2 class="section-heading">
                                        An eye for detail makes our works excellent
                                    </h2>
                                    <!-- End Section Titles -->
                                    
                                    <div class="section-line mb-50 mb-xs-30"></div>
                                    
                                    <!-- Section Text -->
                                    <div class="section-text mb-80 mb-xs-50">
                                        We continuously strived hard and woke up every morning with the commitment to make a positive change to the workplace. As a result of all our tireless efforts, we have successfully managed to develop an exclusive app that allows admin and project leaders to easily:
                                    </div>
                                    <!-- End Section Text -->
                                    
                                </div>
                            </div>
                        </div>
                    <!-- End Portfolio Section -->            
                </div>
            </section>
            <!-- End Services Section -->
            
            <!-- Call Action Section -->
            <section class="page-section bg-dark-alfa-90 bg-scroll" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/section-bg-2.jpg">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <h1 class="section-title large">Looking for exclusive services?</h1>
                            
                            <h2 class="section-heading uppercase strong">Just say hi and we start</h2>
                            <div class="local-scroll">
                                <a href="#contact" class="btn btn-mod btn-large btn-color" target="_blank">Contact Us</a>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End Call Action Section -->            
            
            <!-- Portfolio Section -->
            <section class="page-section pb-0" id="portfolio">
                <div class="relative">
                    
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-2 align-center">
                                <!-- Section Titles -->
                                <h1 class="section-title mb-10">
                                    How Does It Work?
                                </h1>
                                <h2 class="section-heading">
                                    "Simple Outlook, With Much More Under One Roof"
                                </h2>
                                <!-- End Section Titles -->
                                
                                <div class="section-line mb-50 mb-xs-30"></div>
                                
                                <!-- Section Text -->
                                <div class="section-text mb-80 mb-xs-50">
                                    Quipdates has an impressive interface that allows admin and project managers to create and set up their profiles within minutes. But wait, there's much more in stock. Let's try to understand it this way:
                                </div>
                                <!-- End Section Text -->
                                
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 ForAdminProject">
                                <div class="forAdminInner">
                                    <h1 class="section-title mb-10">
                                        For Admin
                                    </h1>
                                    <ul>
                                        <li>
                                            It allows admin to add new projects along with its details and allocating a particular manager to it
                                        </li>
                                        <li>
                                            He has the power to mark a project with a red flag so that its evident that the project needs immediate attention
                                        </li> 
                                        <li>
                                            The admin will be able to add resources too
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 ForAdminProject">
                                <div class="forProjectInner">
                                    <h1 class="section-title mb-10">
                                        For Project Managers   
                                    </h1>
                                    <ul>
                                        <li>
                                            For project managers and leaders, this app is a complete bliss. Its comprehensive dashboard provides the managers with an option of navigating through weekly reports, project lists, alerts, and index projects effortlessly
                                        </li>
                                        <li>
                                            They can also review and edit each project and all its details such as logo, name, allocated resources, etc
                                        </li> 
                                        <li>
                                            Based on priorities, the leaders also have the power to allocate and shift available resources from one project to another
                                        </li>
                                        <li>
                                            The capacity to review index projects and their details is yet another feather in the cap of Quipdates
                                        </li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Portfolio Section -->            
            
            <!-- Call Action Section -->
            <section class="small-section bg-color-alfa-90 bg-scroll" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/section-bg-4.jpg">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <h1 class="section-title large">Like our creative works?</h1>
                            
                            <h2 class="section-heading uppercase strong">We&rsquo;re open to&nbsp;interesting projects</h2>
                            
                            <div class="local-scroll">
                                <a href="#contact" class="btn btn-mod btn-border-w btn-large">Say Hello</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a href="http://vimeo.com/channels/staffpicks/116829150" class="btn btn-mod btn-border-w btn-large lightbox mfp-iframe">Play Reel</a>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End Call Action Section -->
            <!-- Testimonials Section -->
            <section id="testimonial" class="page-section bg-dark bg-dark-alfa-90 fullwidth-slider" data-background="<?php echo Yii::$app->params['live_path'];?>/web/front/images/section-bg-3.jpg">                
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                
                                <!-- Section Title -->
                                <h3 class="section-title large mb-30">What people say?</h3>
                                
                                <blockquote class="testimonial white">
                                    <p>
                                        "Wow. It's just super fun and easy to use Quipdates. I am blown away. Must say, it has an extremely intuitive and effective interface. Now, I can check the availability of available resources with just a tap on my screen".
                                    </p>
                                    <footer class="testimonial-author white mt-30">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
                
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                
                                <!-- Section Title -->
                                <h3 class="section-title large mb-30">What people say?</h3>
                                
                                <blockquote class="testimonial white">
                                    <p>
                                        "You made it super duper simple. This app is so fast and easier to work with. It's like I m blessed with a magic wand that let me smoothly switch between multiple projects and dedicate time equally to every one of it."

                                    </p>
                                    <footer class="testimonial-author white mt-30">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
                
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                
                                <!-- Section Title -->
                                <h3 class="section-title large mb-30">What people say?</h3>
                                
                                <blockquote class="testimonial white">
                                    <p>
                                        "I just wanted to share a quick note and let you know that you guys do a really good job. I am glad I said YES to you. Quipdates is really the best partner I can ever have. It allows me to manage my resources efficiently and be updated on every bit of the projects."
                                    </p>
                                    <footer class="testimonial-author white mt-30">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                
                                <!-- Section Title -->
                                <h3 class="section-title large mb-30">What people say?</h3>
                                
                                <blockquote class="testimonial white">
                                    <p>
                                        "Earlier, I used to struggle through a bunch of notes or excel sheets in order to collaborate with other PMs in the weekly meetings and give them the insights into the projects I handle. But, this product has been a lifesaver for me. It made the tedious task simple as now I can maintain a comprehensive database comprising of all the project related details and issues within the app."
                                    </p>
                                    <footer class="testimonial-author white mt-30">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->                
            </section>
            <!-- End Testimonials Section -->            
            
            <!-- Logotypes Section -->
            <section class="small-section pt-20 pb-20 bg-gray-lighter">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            
                            <div class="small-item-carousel black owl-carousel mb-0 animate-init" data-anim-type="fade-in-right-large" data-anim-delay="100">
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-1.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-2.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-3.png" width="88" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-4.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-5.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-6.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-1.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                                <!-- Logo Item -->
                                <div class="logo-item">
                                    <img src="<?php echo Yii::$app->params['live_path'];?>/web/front/images/client-2.png" width="67" height="67" alt="" />
                                </div>
                                <!-- End Logo Item -->
                                
                            </div>
                                
                         </div>
                     </div>
                    
                 </div>
            </section>
            <!-- End Logotypes -->            
            
            <!-- Contact Section -->
            <section class="page-section" id="contact">
                <div class="container relative">
                    
                    <div class="row mb-80 mb-xs-50">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <!-- Section Titles -->
                            <h1 class="section-title mb-10">
                                Our contact information
                            </h1>
                            <h2 class="section-heading">
                                We&rsquo;re open to&nbsp;talk to&nbsp;nice people
                            </h2>
                            <!-- End Section Titles -->
                            
                            <div class="section-line mb-50 mb-xs-30"></div>
                            
                            <!-- Section Text -->
                            <div class="section-text">
                                Quisque aliquam egestas semper. Phasellus est ipsum, facilisis at pellentesque et, dapibus 
                                at nulla. Morbi risus dolor, lobortis volutpat cursus et, fermentum vitae purus.
                            </div>
                            <!-- End Section Text -->
                            
                        </div>
                    </div>
                    
                    
                    <!-- Contact Information -->
                    <div class="row multi-columns-row alt-features-grid-1">
                        
                        <!-- Contact Item-->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <h3 class="alt-features-title-1">Call us from 10 am to 7 pm</h3>
                                <div class="alt-features-descr-1">
                                    +61 383 765 284
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                        
                        <!-- Contact Item-->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <h3 class="alt-features-title-1">Have a cup of coffee</h3>
                                <div class="alt-features-descr-1">
                                    213 GO Square, Wakad
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                        
                        <!-- Contact Item-->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <h3 class="alt-features-title-1">Just say hello</h3>
                                <div class="alt-features-descr-1 email-link">
                                    <a href="mailto:quipdates@gmail.com">Quipdates@gmail.com</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                    
                    </div>
                    <!-- End Contact Information -->
                </div>
            </section>
            <!-- End Contact Section -->
            
            <!-- Feedback Form Section -->
            <section class="page-section bg-dark contactUs">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <!-- Section Titles -->
                            <h1 class="section-title large">Want to talk?</h1>
                            <h2 class="section-heading mb-40">Just Say Hello</h2>
                            <!-- End Section Titles -->
                            
                        </div>
                    </div>                    
                    
                    <!-- Contact Form -->                            
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            
                            <form class="form contact-form" id="contact_form" autocomplete="off">
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Name -->
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="input-lg form-control" placeholder="Name" pattern=".{3,100}" required>
                                        </div>
                                        
                                        <!-- Email -->
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="input-lg form-control" placeholder="Email" pattern=".{5,100}" required>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Message -->
                                        <div class="form-group">                                            
                                            <textarea name="message" id="message" class="input-lg form-control" style="height: 120px;" placeholder="Message"></textarea>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Inform Tip -->                                        
                                        <div class="form-tip pt-20">
                                            <i class="fa fa-info-circle"></i> All the fields are required
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Send Button -->
                                        <div class="align-right pt-10">
                                            <button class="submit_btn btn btn-mod btn-color btn-large" id="submit_btn">Send Message</button>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                <div id="result"></div>
                            </form>
                            
                        </div>
                    </div>
                    <!-- End Contact Form -->
                    
                </div>
            </section>
            <!-- End Feedback Form Section -->            
            