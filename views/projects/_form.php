<?php

use yii\helpers\Html;
use app\models\WorkDays;
use app\models\Usertype;
use kartik\widgets\Select2;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\widgets\TimePicker;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

     <?php $form = ActiveForm::begin([
      'options' => ['class' => 'form-horiuzontal',
         'enctype' => 'multipart/form-data'
      ],
     'id'      => 'user-form',
     'enableAjaxValidation'   => true,
      'enableClientValidation' => false,
     'fieldConfig' => [
     'template' => "{label}\n<div class=\"col-xs-8\">{input}{hint}{error}</div>",
     'labelOptions' => ['class' => 'col-sm-4 control-label'],
    ],
     ]); ?>
    <div class="row">
        <div class="col-md-12">
             <div class="col-md-6">
 
     <?php echo $form->field($model, 'logo')->widget(FileInput::classname(),[
                  'name' => 'photo',
                   'options'=>[
                    'multiple'=>false,
                    ],
                    'pluginOptions' => [
                      'initialPreview'=>[
                      "".$path."",        
                      ],
                      'browseClass' => 'btn btn-raised btn-primary',
                      'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                      'browseLabel' =>  'Select ',
                      'removeLabel'=>'Remove',
                      'removeClass' => 'btn btn-raised btn-warning',
                      'showRemove' => false,
                      'showUpload' => false,
                      'removeIcon' => '<i class="glyphicon glyphicon-minus"></i> ',
                      'initialPreviewAsData'=>true,
                      'overwriteInitial'=>true,
                              //'maxFileSize'=>1024
                    ]
                                    ]);//->hint('Recent pic of applicant');
                                  ?>

     <?php 
                    echo $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => 'Enter Start date ...'],
                      'pluginOptions' => [
                         'startDate'=> '0d',
                         //'format' =>"",
                          'autoclose'=>true
                      ]
                  ]);?>
                   <?php 
                    echo $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => 'Enter End date ...'],
                      'pluginOptions' => [
                         'startDate'=> '0d',
                         //'format' =>\Yii::$app->formatter->dateFormat,
                          'autoclose'=>true
                      ]
                  ]);?>
   
 
           <?php echo $form->field($model, 'current_index')->widget(Select2::classname(), [
            'data' => [1=>'Yes',0=>'No'],
            'language' => 'en',
              'options' => ['placeholder' => Yii::t('app','Select Index...')],
                'pluginOptions' => [
                 'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
                ]);?>

                    <?php echo $form->field($model, 'work_day_id')->widget(Select2::classname(), [
            'data' =>  WorkDays::getDropDownData(),
            'language' => 'en',
              'options' => ['placeholder' => Yii::t('app','Select Day...')],
                'pluginOptions' => [
                 'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
    ]);?>
    <?= $form->field($model, 'weekly_call_time')->widget(TimePicker::classname(), []);?>
     <?= $form->field($model, 'web_wireframe_link')->textarea(['rows' => 1]) ?>
      
</div>
 <div class="col-md-6">
       <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
      <?= $form->field($model, 'owner')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'platform')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'app_test_case_link')->textarea(['rows' => 1]) ?>
      <?= $form->field($model, 'app_wireframe_link')->textarea(['rows' => 1]) ?>
      <?= $form->field($model, 'app_invision_link')->textarea(['rows' => 1]) ?>
      <?= $form->field($model, 'web_fs_link')->textarea(['rows' => 1]) ?>
      <?= $form->field($model, 'web_invision_link')->textarea(['rows' => 1]) ?>
        <?= $form->field($model, 'web_test_case_link')->textarea(['rows' =>1]) ?>
    
     


</div>
</div>
 
</div>
</div>
  <div class="form-group btn-group">
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

         <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['projects/index'], ['class'=>'btn btn-danger']); ?>
         <?php if($model->isNewRecord): ?>
         
         <button type="reset" class="btn btn-info">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
         </button>
         <?php endif ?>
      </div>
   

    <?php ActiveForm::end(); ?>

</div>
