<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' =>Yii::$app->encryptor->encrypt($model->id)], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' =>Yii::$app->encryptor->encrypt($model->id)], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'name',
            'logo',
            'description:ntext',
             [
             'attribute'=>'start_date',
               'value' => function($model) {
                    return date('m/d/Y',strtotime($model->start_date));
                }
            ],
             [
             'attribute'=>'end_date',
               'value' => function($model) {
                    return date('m/d/Y',strtotime($model->end_date));
                }
            ],
                'weekly_call_time',
            'web_wireframe_link',
            'web_invision_link',
            'web_test_case_link',
            'app_test_case_link',
            'app_wireframe_link',
            'app_invision_link',
            'web_fs_link',
         
        ],
    ]) ?>

</div>
