<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\widgets\Select2;
use app\models\Usertype;



/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php Pjax::begin(); ?>    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'name',
            'logo',
            'description:ntext',
            
            [
             'attribute'=>'start_date',
               'value' => function($model, $key, $index, $column) {
                    return date('m/d/Y',strtotime($model->start_date));
                }
            ],  
            'weekly_call_time',
            'web_wireframe_link',
            'web_invision_link',
            'web_test_case_link',
            'app_test_case_link',
            'app_wireframe_link',
            'app_invision_link',
            'web_fs_link',
          
           

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{update}&nbsp{view}&nbsp{map}',
                    'buttons' => [
                    'map' => function ($url, $model) {
                                  return Html::a('Member', $url, [
                                    'title' => Yii::t('app', 'Map Members'),'class'=>'btn btn-xs btn-primary','target'=>'_blank'
                                 ]);
                              }, 
                  ],

                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['projects/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['projects/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['projects/delete','id'=>$hash]);
                         }else if($action === 'map'){
                          return Url::to(['project-members/create','id'=>Yii::$app->encryptor->encrypt($model->id)]);
                         }
                    },
            ],

        ],

         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i>Projects</h3>',
            'type'=>'primary',
             'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'Projects',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
          ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

<?php Pjax::end(); ?>
  
</div>
